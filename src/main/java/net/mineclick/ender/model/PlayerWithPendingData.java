package net.mineclick.ender.model;

import lombok.Data;
import org.springframework.data.annotation.Id;

@Data
public class PlayerWithPendingData {
    @Id
    private String uuid;
    private String name;
    private PlayerPendingData pendingData;
}

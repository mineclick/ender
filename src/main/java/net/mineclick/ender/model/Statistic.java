package net.mineclick.ender.model;

import lombok.Data;

@Data
public class Statistic {
    private double score = 0;
    private double rank = 9999;
}

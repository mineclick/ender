package net.mineclick.ender.model;

import lombok.Getter;
import lombok.Setter;
import net.mineclick.ender.type.Rank;
import org.springframework.data.annotation.Id;

@Setter
@Getter
public class PlayerId {
    @Id
    private String uuid;
    private boolean online;
    private String server;
    private String name;
    private String rank;
    private int ping;
    private String discordId;
    private String texture;
    private String signature;
    private String createdAt;
    private String updatedAt;

    public boolean isDefault() {
        return Rank.DEFAULT.is(rank) || isPremium() || isStaff() || isSuperStaff() || isDev();
    }

    public boolean isPremium() {
        return Rank.PAID.is(rank) || isStaff() || isSuperStaff() || isDev();
    }

    public boolean isStaff() {
        return Rank.STAFF.is(rank) || isSuperStaff() || isDev();
    }

    public boolean isSuperStaff() {
        return Rank.SUPER_STAFF.is(rank) || isDev();
    }

    public boolean isDev() {
        return Rank.DEV.is(rank);
    }
}


package net.mineclick.ender.model;

import com.google.gson.annotations.SerializedName;
import lombok.Data;

import java.util.Date;

@Data
public class Transaction {
    @SerializedName("_id")
    private String id;
    private String email;
    private String uuid;
    private String name;
    private TransactionItem[] items;
    private boolean processed;
    private boolean cancelled;
    private Date createdAt;
    private Date nextPaymentAt;
    private Date processedAt;
    private Date cancelledAt;
}

package net.mineclick.ender.model;

import lombok.Data;

@Data
public class PlayerPendingData {
    int votes;
}

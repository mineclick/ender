package net.mineclick.ender.model;

import lombok.Data;

@Data
public class AppealStatusForm {
    private String username;
    private OffenceData.Status status;
}

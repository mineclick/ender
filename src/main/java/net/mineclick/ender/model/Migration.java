package net.mineclick.ender.model;

import lombok.Data;
import org.springframework.data.annotation.Id;

import java.util.Date;

@Data
public class Migration {
    @Id
    private final int id;
    private Date executedAt;
}

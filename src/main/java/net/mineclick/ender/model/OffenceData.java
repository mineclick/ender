package net.mineclick.ender.model;

import lombok.Data;
import net.mineclick.ender.type.OffenceType;
import net.mineclick.ender.type.PunishmentType;

import java.time.Instant;

@Data
public class OffenceData {
    private OffenceType type;
    private PunishmentType punishment;
    private int durationMinutes;
    private String punishedBy;
    private String punishedOn;
    private String pardonedBy;
    private String pardonedOn;
    private String deniedBy;
    private String deniedOn;
    private String appealedOn;
    private String appealMessage;

    public String getOffenceReason() {
        return type == null ? "N/A" : type.getReason();
    }

    public boolean shouldShow() {
        if (punishedOn == null) return false;
        Instant punishDate = Instant.parse(punishedOn);

        return durationMinutes < 0 || Instant.now().isBefore(punishDate.plusSeconds(durationMinutes * 60L));
    }

    public Status getStatus() {
        if (pardonedOn != null) return Status.PARDONED;
        if (deniedOn != null) return Status.DENIED;
        if (appealedOn != null) return Status.APPEALED;

        return shouldShow() ? Status.BANNED : Status.EXPIRED;
    }

    public enum Status {
        BANNED,
        APPEALED,
        DENIED,
        PARDONED,
        EXPIRED
    }
}

package net.mineclick.ender.model;

import lombok.Data;

@Data
public class AnnouncementsModel {
    public static final String ID = "announcements";

    private String id = ID;
    private String[] announcements;
    private int interval;
}

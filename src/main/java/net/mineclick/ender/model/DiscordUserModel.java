package net.mineclick.ender.model;

import lombok.Data;

@Data
public class DiscordUserModel {
    private String id;
    private String username;
    private String discriminator;
    private String avatar;
    private String mcUsername;
    private String mcUuid;
}

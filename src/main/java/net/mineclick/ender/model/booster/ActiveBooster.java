package net.mineclick.ender.model.booster;

import lombok.Data;
import net.mineclick.ender.type.BoosterType;

import java.util.Date;
import java.util.List;

@Data
public class ActiveBooster {
    private BoosterType type;
    private List<String> players;
    private Date expiresAt;
    private int duration;
}

package net.mineclick.ender.model.booster;

import lombok.Data;
import net.mineclick.ender.type.BoosterType;

import java.util.Date;

@Data
public class BoosterHistory {
    private BoosterType type;
    private String player;
    private Date activatedAt;
    private Date expiresAt;
}

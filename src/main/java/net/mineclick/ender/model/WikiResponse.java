package net.mineclick.ender.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;

import java.util.List;

@Getter
@JsonIgnoreProperties(ignoreUnknown = true)
public class WikiResponse {
    private Data data;

    @Getter
    public static class Data {
        private Pages pages;
    }

    @Getter
    public static class Pages {
        private Search search;
    }

    @Getter
    public static class Search {
        private List<Result> results;
    }

    @Getter
    public static class Result {
        private String path;
        private String locale;
    }
}

package net.mineclick.ender.model;

import lombok.Data;
import org.springframework.data.annotation.Id;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Data
public class AppealCase {
    @Id
    private String uuid;
    private String name;
    private List<OffenceData> offences;

    public List<OffenceData> getOffences() {
        if (offences != null) {
            return offences.stream()
                    .filter(OffenceData::shouldShow)
                    .sorted(Comparator.<OffenceData>comparingInt(offenceData -> offenceData.getStatus().ordinal()).reversed())
                    .limit(1)
                    .collect(Collectors.toList());
        }

        return null;
    }
}

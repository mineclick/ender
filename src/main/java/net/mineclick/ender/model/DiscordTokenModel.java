package net.mineclick.ender.model;

import lombok.Data;

@Data
public class DiscordTokenModel {
    private String access_token;
}

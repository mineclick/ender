package net.mineclick.ender.model;

import lombok.Data;

@Data
public class MotdModel {
    public static final String ID = "motd";

    private String id = ID;
    private String top;
    private String bottom;
    private String topAppendix;
    private boolean topCentered;
    private boolean bottomCentered;
    private int minVersion;
}

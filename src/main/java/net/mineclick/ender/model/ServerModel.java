package net.mineclick.ender.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import net.mineclick.ender.type.ServerStatus;
import org.springframework.data.annotation.Id;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Date;

@Data
@NoArgsConstructor
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class ServerModel {
    @Id
    @EqualsAndHashCode.Include
    private String id;

    private ServerStatus status;
    private int players;
    private int cpuLoad;
    private double ramUsage;
    private boolean game;
    private Instant startedOn;
    private Instant restartsOn;

    private Date expiresAt = Date.from(Instant.now().plus(1, ChronoUnit.MINUTES));
    private Date noResponseAt = Date.from(Instant.now().plus(10, ChronoUnit.SECONDS));
}

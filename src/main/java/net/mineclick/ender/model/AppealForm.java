package net.mineclick.ender.model;

import lombok.Data;

@Data
public class AppealForm {
    private String username;
    private String message;
}

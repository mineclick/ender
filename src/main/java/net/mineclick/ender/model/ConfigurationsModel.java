package net.mineclick.ender.model;

import lombok.Data;

@Data
public class ConfigurationsModel {
    public static final String ID = "configurations";

    private String id = ID;
    private String configurations;
}

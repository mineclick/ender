package net.mineclick.ender.model;

import lombok.Data;

import java.util.*;

@Data
public class PlayersLog {
    public static final String _id = "playersLog";

    private List<String> log = new ArrayList<>();
    private Date currentTime = new Date();
}

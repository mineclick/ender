package net.mineclick.ender.model;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class ChatData {
    private List<String> history = new ArrayList<>();

    @Data
    public static class Player {
        private ChatData chatData;
    }
}

package net.mineclick.ender.model;

import lombok.Data;

import java.util.Map;

@Data
public class LeaderboardData {
    private final String statistic;
    private final int count;
    private Map<String, Statistic> statistics;
}

package net.mineclick.ender.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class WebUser extends PlayerId {
    private String password;
    private String otpSignature;
    private Status registrationStatus;
    private Status passwordResetStatus;
    private boolean webAccountLocked;
    private boolean webAccountDisabled;
    private String discordState;

    public enum Status {
        AWAITING_OTP,
        VALIDATED_OTP,
        REGISTERED
    }

    @Getter
    public static class PublicWebUser {
        private final String uuid;
        private final String username;
        private final String rank;

        public PublicWebUser(WebUser user) {
            uuid = user.getUuid();
            username = user.getName();
            rank = user.getRank();
        }
    }
}

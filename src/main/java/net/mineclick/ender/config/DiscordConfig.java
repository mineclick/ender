package net.mineclick.ender.config;

import lombok.SneakyThrows;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.JDABuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class DiscordConfig {
    @Value("${discord.token}")
    private String token;

    @Bean
    @SneakyThrows
    public JDA discordApi() {
        if (token.isEmpty())
            return null;
        return JDABuilder.createDefault(token).build();
    }
}

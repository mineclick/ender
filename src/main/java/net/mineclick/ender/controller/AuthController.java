package net.mineclick.ender.controller;

import com.google.common.collect.ImmutableMap;
import lombok.SneakyThrows;
import net.mineclick.core.messenger.Action;
import net.mineclick.ender.component.JwtTokenUtil;
import net.mineclick.ender.component.Throttle;
import net.mineclick.ender.messenger.DiscordLinkHandler;
import net.mineclick.ender.model.*;
import net.mineclick.ender.service.DiscordService;
import net.mineclick.ender.service.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.security.authentication.*;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import javax.servlet.http.HttpServletRequest;
import java.net.URI;
import java.util.Objects;
import java.util.UUID;
import java.util.function.Consumer;

@RestController
public class AuthController {
    private static final String PASSWORD_REGEX = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=\\S+$).{8,}$";
    @Autowired
    private Throttle throttle;
    @Autowired
    private AuthenticationManager authenticationManager;
    @Autowired
    private JwtTokenUtil jwtTokenUtil;
    @Autowired
    private UsersService userDetailsService;
    @Autowired
    private PasswordEncoder passwordEncoder;
    @Autowired
    private HttpServletRequest request;
    @Autowired
    private DiscordService discordService;

    @Value("${discord.apiEndpoint}")
    private String discordApiEndpoint;
    @Value("${discord.clientId}")
    private String discordClientId;
    @Value("${discord.clientSecret}")
    private String discordClientSecret;

    @PostMapping("/api/auth/login")
    public ResponseEntity<Void> login(@RequestBody AuthRequest request) {
        throttle.inc("login", 1);
        if (request.getUsername() == null || request.getPassword() == null) {
            throw new BadCredentialsException("Username and password are required");
        }
        authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(request.getUsername(), request.getPassword()));

        UserDetails userDetails = userDetailsService.loadUserByUsername(request.getUsername());

        ResponseEntity.BodyBuilder builder = ResponseEntity.status(HttpStatus.OK);
        buildTokenCookie(builder, userDetails.getUsername());

        return builder.build();
    }

    @GetMapping("/api/auth/logout")
    public ResponseEntity<Void> logout(@CookieValue("mctoken") String token) {
        throttle.inc("logout", 1);
        String username = jwtTokenUtil.getUsernameFromToken(token);
        if (username == null) return ResponseEntity.badRequest().build();

        userDetailsService.invalidateToken(username);

        ResponseCookie cookie = ResponseCookie.from("mctoken", "")
                .httpOnly(true)
                .path("/api")
                .maxAge(0)
                .build();

        ResponseEntity.BodyBuilder builder = ResponseEntity.status(HttpStatus.OK);
        builder.header(HttpHeaders.SET_COOKIE, cookie.toString());
        return builder.build();
    }

    @PostMapping("/api/auth/register")
    public ResponseEntity<RegisterResponse> register(@RequestBody AuthRequest request) {
        throttle.inc("register", 0.5);
        if (request.getUsername() == null) throw new BadCredentialsException("Username is required");

        WebUser webUser = userDetailsService.getWebUser(request.getUsername());
        if (webUser == null) throw new UsernameNotFoundException("Username not found");
        if (webUser.isWebAccountLocked()) throw new LockedException("Account is locked. Contact staff");
        if (webUser.isWebAccountDisabled()) throw new DisabledException("Account is disabled. Contact staff");

        return passwordHandler(request, webUser, webUser.getRegistrationStatus(), webUser::setRegistrationStatus);
    }

    @PostMapping("/api/auth/resetPassword")
    public ResponseEntity<RegisterResponse> resetPassword(@RequestBody AuthRequest request) {
        if (request.getUsername() == null) throw new BadCredentialsException("Username is required");

        WebUser webUser = userDetailsService.getWebUser(request.getUsername());
        if (webUser == null) throw new UsernameNotFoundException("Username not found");
        if (webUser.isWebAccountDisabled()) throw new DisabledException("Account is disabled. Contact staff");

        WebUser.Status status = webUser.getPasswordResetStatus();
        if (WebUser.Status.REGISTERED.equals(status)) {
            status = null;
        }

        // set a lower throttle rate on the request that will send out otp
        throttle.inc("resetPassword", status == null ? 0.05 : 0.5);

        return passwordHandler(request, webUser, status, webUser::setPasswordResetStatus);
    }

    private ResponseEntity<RegisterResponse> passwordHandler(AuthRequest request, WebUser webUser, WebUser.Status status, Consumer<WebUser.Status> setStatus) {
        ResponseEntity.BodyBuilder builder = ResponseEntity.ok();
        RegisterResponse response = new RegisterResponse();
        if (status == null) {
            status = WebUser.Status.AWAITING_OTP;

            userDetailsService.sendOtpCode(webUser);
        } else {
            switch (status) {
                case AWAITING_OTP:
                    if (request.getOtp() != null) {
                        if (userDetailsService.validateOTPCode(webUser.getName(), request.getOtp())) {
                            webUser.setOtpSignature(UUID.randomUUID().toString());
                            response.setOtpSignature(webUser.getOtpSignature());

                            status = WebUser.Status.VALIDATED_OTP;
                            break;
                        }
                    }

                    response.setError("Invalid code");
                    break;
                case VALIDATED_OTP:
                    if (request.getPassword() != null) {
                        if (webUser.getOtpSignature() == null || !webUser.getOtpSignature().equals(request.getOtpSignature())) {
                            webUser.setOtpSignature(null);
                            status = null;

                            response.setError("Unable to validate your request, please try again");
                            break;
                        }

                        if (isValidPassword(request.getPassword())) {
                            webUser.setPassword(passwordEncoder.encode(request.getPassword()));
                            status = WebUser.Status.REGISTERED;

                            buildTokenCookie(builder, webUser.getName());
                        } else {
                            response.setError("Password is not secure enough");
                        }
                        break;
                    }

                    response.setError("Invalid password");
                    break;
                case REGISTERED:
                    throw new UsernameNotFoundException("Username already registered");
            }
        }

        setStatus.accept(status);
        userDetailsService.updateWebUser(webUser);
        if (status != null) {
            response.setStatus(status.toString());
        }
        return builder.body(response);
    }

    @GetMapping("/api/auth/resendotp")
    public ResponseEntity<Void> resendOtpCode(@RequestParam String username) {
        throttle.inc("otp", 0.05); // 3 times per minute
        WebUser webUser = userDetailsService.getWebUser(username);

        if (webUser == null) throw new UsernameNotFoundException("Username not found");

        if (webUser.isWebAccountDisabled()
                || !WebUser.Status.AWAITING_OTP.equals(webUser.getRegistrationStatus())
                && !WebUser.Status.AWAITING_OTP.equals(webUser.getPasswordResetStatus())
        ) {
            throw new UsernameNotFoundException("OTP code is not available");
        }

        userDetailsService.sendOtpCode(webUser);
        return ResponseEntity.ok().build();
    }

    @SneakyThrows
    @GetMapping("/api/auth/discord")
    public ResponseEntity<DiscordUserModel> linkDiscord(@RequestParam String code, @RequestParam String state) {
        throttle.inc("discord", 1);

        // Find the user by state
        WebUser webUser = userDetailsService.getWebUserByField(ImmutableMap.of("discordState", state));
        if (webUser == null)
            throw new UsernameNotFoundException("Invalid state");

        // Get auth token
        MultiValueMap<String, String> body = new LinkedMultiValueMap<>();
        body.add("client_id", discordClientId);
        body.add("client_secret", discordClientSecret);
        body.add("grant_type", "authorization_code");
        body.add("code", code);
        body.add("scope", "identify");

        String url = request.getHeader("referer");
        url = url.substring(0, url.indexOf("?"));
        body.add("redirect_uri", url);

        URI tokenUri = URI.create(discordApiEndpoint + "/oauth2/token");
        HttpHeaders tokenHeaders = new HttpHeaders();
        tokenHeaders.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        tokenHeaders.add("User-Agent", "Mozilla/5.0");
        HttpEntity<MultiValueMap<String, String>> tokenEntity = new HttpEntity<>(body, tokenHeaders);

        ResponseEntity<DiscordTokenModel> tokenResponse = new RestTemplate()
                .exchange(tokenUri, HttpMethod.POST, tokenEntity, DiscordTokenModel.class);
        String token = Objects.requireNonNull(tokenResponse.getBody()).getAccess_token();

        // Get the user identity
        URI userUri = URI.create(discordApiEndpoint + "/users/@me");
        HttpHeaders userHeaders = new HttpHeaders();
        userHeaders.add("User-Agent", "Mozilla/5.0");
        userHeaders.setBearerAuth(Objects.requireNonNull(token));
        HttpEntity<String> userEntity = new HttpEntity<>("", userHeaders);

        // Update the web user
        ResponseEntity<DiscordUserModel> userResponse = new RestTemplate()
                .exchange(userUri, HttpMethod.GET, userEntity, DiscordUserModel.class);

        if (userResponse.getStatusCode().is2xxSuccessful()) {
            webUser.setDiscordState(null);
            webUser.setDiscordId(Objects.requireNonNull(userResponse.getBody()).getId());
            userDetailsService.updateWebUser(webUser);

            DiscordUserModel discordUser = userResponse.getBody();
            discordUser.setMcUsername(webUser.getName());
            discordUser.setMcUuid(webUser.getUuid());

            discordService.msgUser(discordUser.getId(), ":tada: Successfully linked your Discord account with MineClick!");
            discordService.updateUser(webUser);

            DiscordLinkHandler handler = new DiscordLinkHandler();
            handler.setUuid(webUser.getUuid());
            handler.setDiscordId(webUser.getDiscordId());
            handler.send(Action.POST);

            return ResponseEntity.ok(discordUser);
        }

        return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
    }

    private void buildTokenCookie(ResponseEntity.BodyBuilder builder, String username) {
        String token = userDetailsService.generateToken(username);
        ResponseCookie cookie = ResponseCookie.from("mctoken", token)
                .httpOnly(true)
                .path("/api")
                .maxAge(JwtTokenUtil.LIFETIME)
                .build();

        builder.header(HttpHeaders.SET_COOKIE, cookie.toString());
        builder.header("Authorization", "Bearer " + token);
    }

    private boolean isValidPassword(String password) {
        return password != null && password.matches(PASSWORD_REGEX);
    }
}

package net.mineclick.ender.controller;

import net.mineclick.ender.component.Throttle;
import net.mineclick.ender.service.UsersService;
import net.mineclick.ender.type.Rank;
import org.bson.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.User;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

@RestController
public class PlayersController {
    @Autowired
    private UsersService usersService;
    @Autowired
    private Throttle throttle;

    @GetMapping("/api/players")
    public ResponseEntity<String> getUser(@AuthenticationPrincipal User user, @RequestParam Optional<String> search) {
        String searchValue = search.orElse(null);

        if (!Rank.SUPER_STAFF.hasAuthority(user) || searchValue == null) {
            throttle.inc("getPlayers", 1);
            searchValue = user.getUsername();
        }

        Document document = usersService.searchFullUser(searchValue);
        if (document != null) {
            return ResponseEntity.ok(document.toJson());
        }

        return ResponseEntity.notFound().build();
    }
}

package net.mineclick.ender.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class AppController {
    // "/{path:[^\\.]+}/**" can support sub-paths, but will screw up websockets
    // so if needed to use that, also check for the /api/ inclusion and ignore it
    @RequestMapping("/{path:[^\\.]+}")
    public String forward() {
        return "forward:/";
    }
}

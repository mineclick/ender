package net.mineclick.ender.controller;

import net.mineclick.ender.component.Throttle;
import net.mineclick.ender.model.AppealCase;
import net.mineclick.ender.model.AppealForm;
import net.mineclick.ender.model.AppealStatusForm;
import net.mineclick.ender.service.AppealsService;
import net.mineclick.ender.type.Rank;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.User;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class AppealsController {
    @Autowired
    private AppealsService appealsService;
    @Autowired
    private Throttle throttle;

    @GetMapping("/api/appeals")
    public ResponseEntity<List<AppealCase>> getAppeals(@AuthenticationPrincipal User user) {
        throttle.inc("getAppeals", 1);

        if (!Rank.SUPER_STAFF.hasAuthority(user)) {
            throw new BadCredentialsException("Unauthorized");
        }

        return ResponseEntity.ok(appealsService.getAppeals());
    }

    @GetMapping("/api/appeals/chatHistory")
    public ResponseEntity<List<String>> getChatHistory(@AuthenticationPrincipal User user, String username) {
        throttle.inc("getAppeals", 1);

        if (!Rank.SUPER_STAFF.hasAuthority(user)) {
            throw new BadCredentialsException("Unauthorized");
        }

        return ResponseEntity.ok(appealsService.getChatHistory(username));
    }

    @GetMapping("/api/appeal")
    public ResponseEntity<AppealCase> getAppeal(String username) {
        throttle.inc("getAppeal", 0.2);

        return ResponseEntity.ok(appealsService.getAppeal(username));
    }

    @PostMapping("/api/appeal")
    public ResponseEntity<AppealCase> postAppeal(@RequestBody AppealForm appealForm) {
        throttle.inc("postAppeal", 0.2);

        if (appealForm.getMessage().length() > 1000) {
            return ResponseEntity.badRequest().build();
        }

        return ResponseEntity.ok(appealsService.postAppeal(appealForm.getUsername(), appealForm.getMessage()));
    }

    @PostMapping("/api/appeals/status")
    public ResponseEntity<AppealCase> postAppealStatus(@AuthenticationPrincipal User user, @RequestBody AppealStatusForm appealForm) {
        throttle.inc("postAppeal", 1);

        if (!Rank.SUPER_STAFF.hasAuthority(user)) {
            throw new BadCredentialsException("Unauthorized");
        }

        return ResponseEntity.ok(appealsService.postAppealStatus(user, appealForm.getUsername(), appealForm.getStatus()));
    }
}

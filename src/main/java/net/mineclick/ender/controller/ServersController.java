package net.mineclick.ender.controller;

import lombok.Value;
import net.mineclick.ender.component.Throttle;
import net.mineclick.ender.model.ServerModel;
import net.mineclick.ender.service.ServersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class ServersController {
    @Autowired
    private Throttle throttle;
    @Autowired
    private ServersService serversService;

    @GetMapping("/api/servers")
    public ResponseEntity<ServersResponse> getServers() {
        throttle.inc("servers", 0.5);
        ServersResponse response = new ServersResponse(serversService.getServers(), serversService.getPlayersLog());
        return ResponseEntity.ok().body(response);
    }

    @Value
    public static class ServersResponse {
        List<ServerModel> servers;
        List<String> playersLog;
    }
}

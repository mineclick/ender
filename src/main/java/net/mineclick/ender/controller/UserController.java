package net.mineclick.ender.controller;

import net.mineclick.ender.model.WebUser;
import net.mineclick.ender.service.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserController {
    @Autowired
    private UsersService usersService;

    @GetMapping("/api/user")
    public ResponseEntity<WebUser.PublicWebUser> getUser(@AuthenticationPrincipal User user) {
        WebUser webUser = usersService.getWebUser(user.getUsername());
        if (webUser == null) throw new UsernameNotFoundException("User not found");

        return ResponseEntity.ok().body(new WebUser.PublicWebUser(webUser));
    }
}

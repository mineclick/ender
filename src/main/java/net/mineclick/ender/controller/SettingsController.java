package net.mineclick.ender.controller;

import net.mineclick.ender.model.ConfigurationsModel;
import net.mineclick.ender.model.MotdModel;
import net.mineclick.ender.model.AnnouncementsModel;
import net.mineclick.ender.service.SettingsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SettingsController {
    @Autowired
    private SettingsService settingsService;

    @GetMapping("/api/settings/motd")
    public ResponseEntity<MotdModel> getMotd() {
        return ResponseEntity.ok(settingsService.getMotd());
    }

    @PostMapping("/api/settings/motd")
    public ResponseEntity<Void> saveMotd(@RequestBody MotdModel motd) {
        settingsService.saveMotd(motd);
        return ResponseEntity.ok().build();
    }

    @GetMapping("/api/settings/announcements")
    public ResponseEntity<AnnouncementsModel> getAnnouncements() {
        return ResponseEntity.ok(settingsService.getAnnouncements());
    }

    @PostMapping("/api/settings/announcements")
    public ResponseEntity<Void> saveAnnouncements(@RequestBody AnnouncementsModel announcements) {
        settingsService.saveAnnouncements(announcements);
        return ResponseEntity.ok().build();
    }

    @GetMapping("/api/settings/configurations")
    public ResponseEntity<ConfigurationsModel> getConfigurations() {
        return ResponseEntity.ok(settingsService.getConfigurations());
    }

    @PostMapping("/api/settings/configurations")
    public ResponseEntity<Void> saveConfigurations(@RequestBody ConfigurationsModel configurations) {
        settingsService.saveConfigurations(configurations);
        return ResponseEntity.ok().build();
    }
}

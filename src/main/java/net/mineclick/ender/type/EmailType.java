package net.mineclick.ender.type;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public enum EmailType {
    STORE_RECEIPT("d-f410eb4179f7409cb3ad37e1b7d3753f", "store.noreply@mineclick.net", "MineClick Store");

    private final String templateId;
    private final String fromEmail;
    private final String fromName;

    EmailType(String templateId) {
        this.templateId = templateId;
        fromEmail = "noreply@mineclick.net";
        fromName = "MineClick";
    }
}

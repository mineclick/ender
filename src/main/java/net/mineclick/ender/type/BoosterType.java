package net.mineclick.ender.type;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public enum BoosterType {
    GOLD_BOOSTER(
            "Gold Income Booster",
            "potion_gold"
    ),
    PICKAXE_BOOSTER(
            "Pickaxe Income Booster",
            "potion_red"
    ),
    POWERUPS_BOOSTER(
            "Powerups Booster",
            "potion_teal"
    ),
    SUPER_BLOCK_BOOSTER(
            "Super Block Booster",
            "potion_pink"
    ),
    CHEAP_WORKER_BOOSTER(
            "Cheap Workers Booster",
            "potion_green"
    ),
    CHEAP_BUILDINGS_BOOSTER(
            "Cheap Buildings Booster",
            "potion_blue"
    );

    private final String name;
    private final String emote;
}

package net.mineclick.ender.type;

public enum ServerStatus {
    STARTING,
    RUNNING,
    WAITING_TO_RESTART,
    RESTARTING,
    NO_RESPONSE
}

package net.mineclick.ender.type;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public enum OffenceType {
    FLYING("flying"),
    AUTOCLICKING("autoclicking"),
    SWEARING("swearing"),
    HARASSING("harassing/bullying"),
    AVOIDING_AFK("avoiding afk detection"),
    SPAMMING("spamming/advertising"),
    IMPERSONATING_STAFF("impersonating staff"),
    OTHER("being a baddie");

    private final String reason;
}

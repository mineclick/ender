package net.mineclick.ender.type;

import lombok.Getter;
import org.springframework.security.core.userdetails.User;

import java.util.ArrayList;
import java.util.List;

@Getter
public enum Rank {
    // Order of the enums is important
    DEFAULT,
    PAID,
    STAFF,
    SUPER_STAFF,
    DEV;

    private final String springRole;

    Rank() {
        this.springRole = "ROLE_" + toString();
    }

    /**
     * Check if the rank is the same
     *
     * @param rank The rank
     * @return True if the rank is the same
     */
    public boolean is(String rank) {
        return toString().equals(rank);
    }

    /**
     * Returns a list of ranks that this rank is inheriting.
     * The list includes the rank itself
     *
     * @return List of (strings) inherited ranks
     */
    public String[] getSubRanks() {
        List<String> ranks = new ArrayList<>();

        for (Rank rank : values()) {
            if (rank.equals(this)) {
                ranks.add(rank.name());
                return ranks.toArray(new String[0]);
            } else {
                ranks.add(rank.name());
            }
        }

        return null;
    }

    /**
     * Check if the user has this rank in its authorities
     *
     * @param user The user
     * @return True if the user has the authority
     */
    public boolean hasAuthority(User user) {
        return user != null
                && user.getAuthorities().stream().anyMatch(authority -> authority.getAuthority().equals(getSpringRole()));
    }
}

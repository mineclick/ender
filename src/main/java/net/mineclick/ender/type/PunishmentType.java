package net.mineclick.ender.type;

public enum PunishmentType {
    WARNING,
    MUTE,
    KICK,
    BAN
}

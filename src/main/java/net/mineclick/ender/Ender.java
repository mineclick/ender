package net.mineclick.ender;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@EnableScheduling
@SpringBootApplication
public class Ender {
    public final static Logger logger = LoggerFactory.getLogger(Ender.class);

    public static void main(String... args) {
        SpringApplication.run(Ender.class, args);
    }
}

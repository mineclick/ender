package net.mineclick.ender.component;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.Keys;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.ReactiveRedisTemplate;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import java.time.Duration;
import java.time.Instant;
import java.util.Base64;
import java.util.Date;
import java.util.function.Function;

@Component
public class JwtTokenUtil {
    public static final Duration LIFETIME = Duration.ofDays(30);
    private static final SignatureAlgorithm ALGORITHM = SignatureAlgorithm.HS512;

    private SecretKey cachedSecretKey;
    private Instant secretCacheExpire;

    @Autowired
    private ReactiveRedisTemplate<String, String> redisTemplate;

    /**
     * Get the username from the JWT
     *
     * @param token The token
     * @return The username or <b>null</b> if the token is invalid
     */
    public String getUsernameFromToken(String token) {
        return getClaimFromToken(token, Claims::getSubject);
    }

    /**
     * Get the expiration date from the JWT
     *
     * @param token The token
     * @return The expiration date or <b>null</b> if the token is invalid
     */
    public Date getExpirationDateFromToken(String token) {
        return getClaimFromToken(token, Claims::getExpiration);
    }

    /**
     * Get a claim from the token by applying a resolver function to it
     *
     * @param token          The JWT
     * @param claimsResolver The resolver
     * @param <T>            Resolver return type
     * @return The resolved claim or <b>null</b> if the token is invalid
     */
    public <T> T getClaimFromToken(String token, Function<Claims, T> claimsResolver) {
        try {
            Claims claims = Jwts.parserBuilder().setSigningKey(getSecret()).build().parseClaimsJws(token).getBody();
            return claimsResolver.apply(claims);
        } catch (JwtException e) {
            return null;
        }
    }

    /**
     * Generate a new token from the user details
     *
     * @param username The username
     * @return A new JWT
     */
    public String generateToken(String username) {
        return Jwts.builder()
                .setSubject(username)
                .setIssuedAt(new Date(System.currentTimeMillis()))
                .setExpiration(new Date(System.currentTimeMillis() + LIFETIME.toMillis()))
                .signWith(getSecret())
                .compact();
    }

    /**
     * Validate if the token is valid
     *
     * @param token       The JWT
     * @param userDetails The user details to validate against
     * @return True if the token is validated against the user details (and the token itself is valid)
     */
    public boolean validateToken(String token, UserDetails userDetails) {
        String username = getUsernameFromToken(token);
        return username != null
                && username.equals(userDetails.getUsername())
                && !isTokenExpired(token);
    }

    private boolean isTokenExpired(String token) {
        Date expiration = getExpirationDateFromToken(token);
        return expiration == null || expiration.before(new Date());
    }

    private SecretKey getSecret() {
        if (cachedSecretKey != null
                && secretCacheExpire != null
                && secretCacheExpire.isAfter(Instant.now())) {
            return cachedSecretKey;
        }

        String key = "ender:jwtSecret";
        String encoded = redisTemplate.opsForValue().get(key).block();
        if (encoded == null) {
            cachedSecretKey = Keys.secretKeyFor(ALGORITHM);
            encoded = Base64.getEncoder().encodeToString(cachedSecretKey.getEncoded());
            redisTemplate.opsForValue().set(key, encoded)
                    .subscribe(aBoolean -> redisTemplate.expire(key, LIFETIME).subscribe());
        } else {
            byte[] decoded = Base64.getDecoder().decode(encoded);
            cachedSecretKey = new SecretKeySpec(decoded, ALGORITHM.getJcaName());
        }

        secretCacheExpire = Instant.now().plus(Duration.ofMinutes(1));
        return cachedSecretKey;
    }
}

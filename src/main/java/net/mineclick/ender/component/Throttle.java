package net.mineclick.ender.component;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.google.common.util.concurrent.RateLimiter;
import lombok.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.context.request.RequestContextHolder;

import javax.annotation.Nonnull;
import java.time.Duration;

@Component
@SuppressWarnings("UnstableApiUsage")
public class Throttle {
    private final LoadingCache<RateKey, RateLimiter> cache = CacheBuilder.newBuilder()
            .expireAfterAccess(Duration.ofMinutes(5))
            .build(new CacheLoader<RateKey, RateLimiter>() {
                @Override
                public RateLimiter load(@Nonnull RateKey rateKey) {
                    return RateLimiter.create(rateKey.rate);
                }
            });

    public void inc(String request, double permitsPerSecond) {
        String key = RequestContextHolder.currentRequestAttributes().getSessionId() + request;
        RateLimiter limiter = cache.getUnchecked(new RateKey(key, permitsPerSecond));
        if (!limiter.tryAcquire())
            throw new TooManyRequestsException();
    }

    @ResponseStatus(HttpStatus.TOO_MANY_REQUESTS)
    public static class TooManyRequestsException extends RuntimeException {
        public TooManyRequestsException() {
            super("Too many requests");
        }
    }

    @Value
    static private class RateKey {
        private String key;
        private double rate;
    }
}

package net.mineclick.ender.component;

import lombok.Getter;
import net.mineclick.core.messenger.Messenger;
import net.mineclick.core.messenger.redisWrapper.LettuceWrapper;
import net.mineclick.ender.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.redis.core.ReactiveRedisTemplate;
import org.springframework.data.redis.listener.ReactiveRedisMessageListenerContainer;
import org.springframework.stereotype.Component;

@Getter
@Component
public class StaticServiceProvider {
    private static StaticServiceProvider i;
    private Messenger messenger;
    @Autowired
    private MongoTemplate mongoTemplate;
    @Autowired
    private MongoUpdater mongoUpdater;
    @Autowired
    private ServersService serversService;
    @Autowired
    private SettingsService settingsService;
    @Autowired
    private StatisticsService statisticsService;
    @Autowired
    private LeaderboardsService leaderboardsService;
    @Autowired
    private BoostersService boostersService;
    @Autowired
    private UsersService usersService;
    @Autowired
    private EmailServices emailServices;
    @Autowired
    private ReactiveRedisTemplate<String, String> reactiveRedisTemplate;
    @Autowired
    private ReactiveRedisMessageListenerContainer listenerContainer;
    @Autowired
    private DiscordService discordService;

    public static StaticServiceProvider i() {
        return i;
    }

    @EventListener(ContextRefreshedEvent.class)
    public void contextRefreshedEvent() {
        i = this;

        if (messenger == null) {
            messenger = new Messenger("ender", new LettuceWrapper(reactiveRedisTemplate, listenerContainer));
        }

        usersService.initMongo();
    }
}

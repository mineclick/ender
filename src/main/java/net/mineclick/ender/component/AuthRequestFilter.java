package net.mineclick.ender.component;

import lombok.SneakyThrows;
import net.mineclick.ender.service.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.annotation.Nonnull;
import javax.servlet.FilterChain;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Arrays;

@Component
public class AuthRequestFilter extends OncePerRequestFilter {
    @Autowired
    private UsersService usersService;
    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    @SneakyThrows
    @Override
    protected void doFilterInternal(@Nonnull HttpServletRequest request, @Nonnull HttpServletResponse response, @Nonnull FilterChain chain) {
        if (SecurityContextHolder.getContext().getAuthentication() == null) {
            String token;
            Cookie[] cookies = request.getCookies();
            if (cookies == null) {
                cookies = new Cookie[0];
            }
            Cookie cookie = Arrays.stream(cookies).filter(c -> c.getName().equals("mctoken")).findFirst().orElse(null);

            if (cookie != null) {
                token = cookie.getValue();
            } else {
                token = request.getHeader("Authorization");
                token = token != null && token.startsWith("Bearer ") ? token.substring(7) : null;
            }

            if (token != null) {
                String username = jwtTokenUtil.getUsernameFromToken(token);

                if (usersService.isTokenValid(username, token)) {
                    if (username != null) {
                        UserDetails userDetails = usersService.loadUserByUsername(username);
                        if (jwtTokenUtil.validateToken(token, userDetails)) {
                            UsernamePasswordAuthenticationToken authToken = new UsernamePasswordAuthenticationToken(userDetails, null, userDetails.getAuthorities());
                            authToken.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
                            SecurityContextHolder.getContext().setAuthentication(authToken);
                        }
                    }
                }
            }
        }
        chain.doFilter(request, response);
    }
}
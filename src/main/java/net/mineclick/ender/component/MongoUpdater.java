package net.mineclick.ender.component;

import lombok.SneakyThrows;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Component;

import java.lang.reflect.Field;

@Component
public class MongoUpdater {
    @SneakyThrows
    public Update getUpdateFor(Object object) {
        Update update = new Update();
        for (Field field : object.getClass().getDeclaredFields()) {
            field.setAccessible(true);
            update.set(field.getName(), field.get(object));
        }

        return update;
    }
}

package net.mineclick.ender.utils;

public final class Constants {
    public final static String PLAYERS_DB = "players";
    public final static String PLAYERS_SNAPSHOT_DB = "players_snapshot";
    public final static String PLAYERS_LOG = "players_log";
    public final static String TRANSACTIONS_DB = "transactions";
    public final static String SETTINGS_DB = "settings";
    public final static String BOOSTERS_DB = "boosters";
    public final static String SERVERS_DB = "servers";
    public final static String MIGRATIONS = "migrations";
}

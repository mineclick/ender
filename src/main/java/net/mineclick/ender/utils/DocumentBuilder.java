package net.mineclick.ender.utils;

import org.bson.Document;

import java.util.HashMap;
import java.util.Map;

public class DocumentBuilder {
    private Map<String, Object> elements = new HashMap<>();

    public static DocumentBuilder start() {
        return new DocumentBuilder();
    }

    public static DocumentBuilder start(String key, Object value) {
        DocumentBuilder builder = new DocumentBuilder();
        builder.elements.put(key, value);
        return builder;
    }

    public DocumentBuilder add(String key, Object value) {
        elements.put(key, value);
        return this;
    }

    public Document get() {
        return new Document(elements);
    }
}

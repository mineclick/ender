package net.mineclick.ender.service;

import com.mongodb.client.AggregateIterable;
import com.mongodb.client.FindIterable;
import net.mineclick.ender.Ender;
import net.mineclick.ender.model.Migration;
import net.mineclick.ender.utils.Constants;
import org.bson.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.lang.reflect.Method;
import java.util.*;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

@Service
public class MigrationService {
    @Autowired
    private MongoTemplate mongoTemplate;

    @PostConstruct
    private void postConstructor() {
        Executors.newScheduledThreadPool(1).schedule(this::executeMigrations, 10, TimeUnit.SECONDS);
    }

    private void executeMigrations() {
        Map<Integer, Method> migrations = new LinkedHashMap<>();
        for (Method method : this.getClass().getDeclaredMethods()) {
            method.setAccessible(true);
            if (method.getName().contains("_")) {
                String[] split = method.getName().split("_");
                migrations.put(Integer.parseInt(split[1]), method);
            }
        }

        List<Integer> migratedIds = mongoTemplate.findAll(Migration.class, Constants.MIGRATIONS)
                .stream().map(Migration::getId).collect(Collectors.toList());
        migrations.keySet().removeAll(migratedIds);

        // TODO apparently this shit is not sorted (or at least not inserted in the right order)
        for (Map.Entry<Integer, Method> entry : migrations.entrySet()) {
            Ender.logger.info("Executing migration #" + entry.getKey());
            try {
                Object completed = entry.getValue().invoke(this);
                if (completed instanceof Boolean && !(Boolean) completed) {
                    throw new Exception("Migration #" + entry.getKey() + " returned false");
                }
            } catch (Exception e) {
                Ender.logger.error("Error running migration #" + entry.getKey(), e);
                Ender.logger.error("No other migration will be attempted to run");
                return;
            }

            Migration migration = new Migration(entry.getKey());
            migration.setExecutedAt(new Date());
            mongoTemplate.insert(migration, Constants.MIGRATIONS);
            Ender.logger.info("Migration #" + migration.getId() + " is complete.");
        }
    }

    private boolean fixBannedUsers_1() {
        FindIterable<Document> documents = mongoTemplate.getCollection(Constants.PLAYERS_DB).find(Document.parse("{\"offences.punishedBy\": \"Unknown\"}"));
        for (Document document : documents) {
            try {
                Document offence = document.get("offences", Document.class);
                Update update = Update.update("offences", Collections.singleton(offence));
                mongoTemplate.updateFirst(Query.query(Criteria.where("_id").is(document.get("_id"))), update, Constants.PLAYERS_DB);
            } catch (Exception e) {
                System.out.println("Skipped: " + document.get("_id"));
            }
        }

        return true;
    }

    /**
     * Change the way powerup parts are being stored.
     * Easy steps to create this aggregation:
     * - Use MongoDB Compass
     * - Create your aggregation
     * - Export to JAVA with the "Use Builders" setting turned off
     * - Success.
     */
    private boolean powerupsMigration_2() {
        List<Document> documents = Arrays.asList(new Document("$project", new Document("powerups", new Document("$objectToArray", "$powerups"))),
                new Document("$group", new Document("_id", "$_id").append("powerupParts", new Document("$sum", new Document("$sum", "$powerups.v.parts")))),
                new Document("$match", new Document("powerupParts", new Document("$gt", 0L))),
                new Document("$sort", new Document("powerupParts", -1L)));
        AggregateIterable<Document> response = mongoTemplate.getCollection(Constants.PLAYERS_DB).aggregate(documents);
        for (Document document : response) {
            double parts = (double) document.get("powerupParts");

            int legendary = parts >= 1000 ? (int) (parts / 1000) : 0;
            int superRare = parts >= 100 ? (int) Math.min(parts / 300, 5) : 0;
            int rare = parts >= 50 ? (int) Math.min(parts / 100, 10) : 0;
            int uncommon = parts >= 20 ? (int) Math.min(parts / 50, 15) : 0;
            int common = parts >= 10 ? (int) Math.min(parts / 20, 20) : 0;

            Map<String, Integer> geodes = new HashMap<>();
            geodes.put("LEGENDARY", legendary);
            geodes.put("VERY_RARE", superRare);
            geodes.put("RARE", rare);
            geodes.put("UNCOMMON", uncommon);
            geodes.put("COMMON", common);

            Update update = Update
                    .update("powerupParts", parts)
                    .unset("powerups")
                    .unset("pickaxePowerup.type")
                    .unset("pickaxePowerup.unlocked")
                    .unset("pickaxePowerup.enabled")
                    .set("geodes", geodes);
//                    .set("unlockedPowerups", Collections.singleton("AUTOCLICKER"));
            mongoTemplate.updateFirst(Query.query(Criteria.where("_id").is(document.get("_id"))), update, Constants.PLAYERS_DB);
        }

        return true;
    }
}

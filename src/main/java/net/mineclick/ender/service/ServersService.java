package net.mineclick.ender.service;

import net.mineclick.ender.component.MongoUpdater;
import net.mineclick.ender.model.PlayersLog;
import net.mineclick.ender.model.ServerModel;
import net.mineclick.ender.type.ServerStatus;
import net.mineclick.ender.utils.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class ServersService {
    private static final int LOG_INTERVAL = 30; // 30 seconds

    @Autowired
    private MongoTemplate mongoTemplate;
    @Autowired
    private MongoUpdater mongoUpdater;

    public void handle(ServerModel server) {
        Update update = mongoUpdater.getUpdateFor(server);
        mongoTemplate.upsert(Query.query(Criteria.where("_id").is(server.getId())), update, Constants.SERVERS_DB);
    }

    public List<ServerModel> getServers() {
        List<ServerModel> servers = mongoTemplate.findAll(ServerModel.class, Constants.SERVERS_DB);
        Date now = new Date();
        return servers.stream()
                .filter(serverModel -> serverModel.getExpiresAt().after(now))
                .peek(serverModel -> {
                    if (serverModel.getNoResponseAt().before(now)) {
                        serverModel.setStatus(ServerStatus.NO_RESPONSE);
                    }
                })
                .collect(Collectors.toList());
    }

    public List<String> getPlayersLog() {
        PlayersLog log = mongoTemplate.findById(PlayersLog._id, PlayersLog.class, Constants.PLAYERS_LOG);
        if (log == null) {
            return new ArrayList<>();
        }

        return log.getLog();
    }

    @Scheduled(fixedRate = 10000)
    public void update() {
        mongoTemplate.remove(Query.query(Criteria.where("expiresAt").lte(new Date())), Constants.SERVERS_DB);
        mongoTemplate.updateMulti(Query.query(Criteria.where("noResponseAt").lte(new Date())), Update.update("status", ServerStatus.NO_RESPONSE), Constants.SERVERS_DB);

        // update players log
        List<ServerModel> servers = getServers().stream()
                .filter(s -> !s.getStatus().equals(ServerStatus.NO_RESPONSE))
                .collect(Collectors.toList());
        int count = servers.stream().mapToInt(ServerModel::getPlayers).sum();

        PlayersLog playersLog = mongoTemplate.findById(PlayersLog._id, PlayersLog.class, Constants.PLAYERS_LOG);
        if (playersLog == null) {
            playersLog = new PlayersLog();
        }

        List<String> log = playersLog.getLog();
        if (log.isEmpty()) {
            log.add(Instant.now().toString() + " " + count);
        } else {
            if (playersLog.getCurrentTime().after(Date.from(Instant.now().minus(LOG_INTERVAL, ChronoUnit.SECONDS)))) {
                String[] entry = log.get(log.size() - 1).split(" ");
                log.set(log.size() - 1, entry[0] + " " + count);
            } else {
                playersLog.setCurrentTime(new Date());

                log.add(Instant.now().toString() + " " + count);
                if (log.size() >= (60 / LOG_INTERVAL) * 60 * 24) { // 30 sec interval for 24 hours
                    log.remove(0);
                }
            }
        }

        mongoTemplate.upsert(Query.query(Criteria.where("_id").is(PlayersLog._id)), mongoUpdater.getUpdateFor(playersLog), Constants.PLAYERS_LOG);
    }
}

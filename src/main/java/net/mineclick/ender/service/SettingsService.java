package net.mineclick.ender.service;

import net.mineclick.core.messenger.Action;
import net.mineclick.ender.messenger.SettingsHandler;
import net.mineclick.ender.model.AnnouncementsModel;
import net.mineclick.ender.model.ConfigurationsModel;
import net.mineclick.ender.model.MotdModel;
import net.mineclick.ender.utils.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Service;

@Service
public class SettingsService {
    @Autowired
    private MongoTemplate mongoTemplate;

    /**
     * Queries the db and returns the {@link MotdModel}
     *
     * @return The {@link MotdModel}
     */
    public MotdModel getMotd() {
        return mongoTemplate.findById(MotdModel.ID, MotdModel.class, Constants.SETTINGS_DB);
    }

    /**
     * Saves the MotdModel to the db.
     * Also broadcasts the update to Messenger
     *
     * @param motdModel The MotdModel
     */
    public void saveMotd(MotdModel motdModel) {
        mongoTemplate.save(motdModel, Constants.SETTINGS_DB);

        updateMessenger(motdModel);
    }

    /**
     * Query the db and returns the {@link AnnouncementsModel}
     *
     * @return The {@link AnnouncementsModel}
     */
    public AnnouncementsModel getAnnouncements() {
        return mongoTemplate.findById(AnnouncementsModel.ID, AnnouncementsModel.class, Constants.SETTINGS_DB);
    }

    /**
     * Saves the AnnouncementsModel to the db.
     * Also broadcasts the update to Messenger
     *
     * @param announcementsModel The AnnouncementsModel
     */
    public void saveAnnouncements(AnnouncementsModel announcementsModel) {
        mongoTemplate.save(announcementsModel, Constants.SETTINGS_DB);

        updateMessenger(announcementsModel);
    }

    /**
     * Query the db and return the {@link ConfigurationsModel}
     *
     * @return The {@link ConfigurationsModel}
     */
    public ConfigurationsModel getConfigurations() {
        return mongoTemplate.findById(ConfigurationsModel.ID, ConfigurationsModel.class, Constants.SETTINGS_DB);
    }

    /**
     * Saves the ConfigurationsModel to the db.
     * Also broadcasts the update to Messenger
     *
     * @param configurationsModel The ConfigurationsModel
     */
    public void saveConfigurations(ConfigurationsModel configurationsModel) {
        mongoTemplate.save(configurationsModel, Constants.SETTINGS_DB);

        updateMessenger(configurationsModel);
    }

    private void updateMessenger(AnnouncementsModel model) {
        updateMessenger(model, null, null);
    }

    private void updateMessenger(MotdModel model) {
        updateMessenger(null, model, null);
    }

    private void updateMessenger(ConfigurationsModel model) {
        updateMessenger(null, null, model);
    }

    private void updateMessenger(AnnouncementsModel announcements, MotdModel motd, ConfigurationsModel configurations) {
        if (announcements == null) {
            announcements = getAnnouncements();
        }
        if (motd == null) {
            motd = getMotd();
        }
        if (configurations == null) {
            configurations = getConfigurations();
        }

        SettingsHandler handler = new SettingsHandler();
        handler.setAnnouncements(announcements);
        handler.setMotd(motd);
        handler.setConfigurations(configurations);
        handler.send(Action.UPDATE);
    }
}

package net.mineclick.ender.service;

import net.mineclick.ender.model.Statistic;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.ReactiveRedisTemplate;
import org.springframework.data.redis.core.ReactiveZSetOperations;
import org.springframework.data.util.Pair;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.function.Predicate;

@Service
public class StatisticsService {
    @Autowired
    private ReactiveRedisTemplate<String, String> redisTemplate;

    /**
     * Get statistics (both score and rank) for a given player
     *
     * @param uuid The player's uuid
     * @param keys Collection of statistic keys
     * @return A mapping of the statistic value to its key
     */
    public Mono<Map<String, Statistic>> getStatistics(UUID uuid, Set<String> keys) {
        ReactiveZSetOperations<String, String> ops = redisTemplate.opsForZSet();

        return Flux.fromIterable(keys)
                .flatMap(key -> ops.score("server.statistics:" + key, uuid.toString())
                        .concatWith(ops.rank("server.statistics:" + key, uuid.toString()).map(aLong -> (double) aLong))
                        .collectList()
                        .map(list -> {
                            Statistic statistic = new Statistic();
                            statistic.setScore(list.isEmpty() ? 0 : list.get(0));
                            statistic.setRank(list.isEmpty() ? 999 : list.get(1));
                            return Pair.of(key, statistic);
                        })
                ).collectMap(Pair::getFirst, Pair::getSecond);
    }

    /**
     * Set a collection of statistics for a given player.
     * The values of the statistics map will be mutated with the updated rank
     *
     * @param uuid       The player's uuid
     * @param statistics The statistics set
     * @return A flux of successful operations
     */
    public Mono<Boolean> setStatistics(UUID uuid, Map<String, Statistic> statistics) {
        ReactiveZSetOperations<String, String> ops = redisTemplate.opsForZSet();

        return Flux.fromIterable(statistics.entrySet())
                .flatMap(entry -> {
                    String key = "server.statistics:" + entry.getKey();
                    return ops.add(key, uuid.toString(), entry.getValue().getScore())
                            .flatMap(a -> ops.rank(key, uuid.toString()).flatMap(rank -> {
                                entry.getValue().setRank(rank);
                                return Mono.just(a);
                            }));
                }).all(Predicate.isEqual(true));
    }
}

package net.mineclick.ender.service;

import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import com.mongodb.client.result.UpdateResult;
import lombok.Getter;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.entities.*;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;
import net.dv8tion.jda.api.events.message.guild.react.GuildMessageReactionAddEvent;
import net.dv8tion.jda.api.events.message.priv.PrivateMessageReceivedEvent;
import net.dv8tion.jda.api.exceptions.HierarchyException;
import net.dv8tion.jda.api.hooks.EventListener;
import net.mineclick.core.messenger.Action;
import net.mineclick.core.messenger.Messenger;
import net.mineclick.ender.messenger.DiscordLinkHandler;
import net.mineclick.ender.messenger.DiscordNotifyHandler;
import net.mineclick.ender.messenger.OffencesHandler;
import net.mineclick.ender.model.OffenceData;
import net.mineclick.ender.model.PlayerId;
import net.mineclick.ender.model.WikiResponse;
import net.mineclick.ender.type.OffenceType;
import net.mineclick.ender.type.PunishmentType;
import net.mineclick.ender.utils.Constants;
import net.mineclick.ender.utils.RandomBotMessages;
import org.bson.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.data.redis.core.ReactiveRedisTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import javax.annotation.PostConstruct;
import java.time.Duration;
import java.time.Instant;
import java.util.*;
import java.util.function.Consumer;

@Service
public class DiscordService {
    @Value("${discord.serverId}")
    private String serverId;
    @Value("${mineclick.wiki-token}")
    private String wikiToken;
    @Getter
    @Value("${discord.gameChatChannelName}")
    private String gameChatChannelName;
    @Getter
    @Value("${discord.reportsChannelName}")
    private String reportsChannelName;
    @Autowired(required = false)
    private JDA discordApi;
    @Autowired
    private MongoTemplate mongoTemplate;
    @Autowired
    private ReactiveRedisTemplate<String, String> redisTemplate;
    @Autowired
    private UsersService usersService;

    @PostConstruct
    private void initListener() {
        if (discordApi == null) return;

        discordApi.addEventListener((EventListener) event -> {
            if (event instanceof PrivateMessageReceivedEvent) {
                PrivateMessageReceivedEvent messageEvent = (PrivateMessageReceivedEvent) event;
                if (messageEvent.getMessage().getContentRaw().equalsIgnoreCase("/unlink")) {
                    UpdateResult updateResult = mongoTemplate.updateFirst(Query.query(Criteria.where("discordId").is(messageEvent.getAuthor().getId())), new Update().unset("discordId"), Constants.PLAYERS_DB);
                    if (updateResult.getModifiedCount() > 0) {
                        String id = messageEvent.getAuthor().getId();
                        msgUser(id, "**Your Discord account has been unlinked from MineClick**");

                        DiscordLinkHandler handler = new DiscordLinkHandler();
                        handler.setDiscordId(id);
                        handler.send(Action.DELETE);

                        clearRoles(id);
                    }
                }
            } else if (event instanceof GuildMessageReceivedEvent) {
                GuildMessageReceivedEvent receivedEvent = (GuildMessageReceivedEvent) event;
                if (receivedEvent.getGuild().getId().equals(serverId)) {
                    String message = receivedEvent.getMessage().getContentRaw().toLowerCase().split(" ")[0];
                    switch (message) {
                        case "hey":
                            handleHeyMessage(receivedEvent);
                            break;
                        case "/mute":
                            handleMuteCommand(receivedEvent);
                            break;
                        case "/wiki":
                            handleWikiSearch(receivedEvent);
                            break;
                    }
                }
            } else if (event instanceof GuildMessageReactionAddEvent) {
                GuildMessageReactionAddEvent reactionAddEvent = (GuildMessageReactionAddEvent) event;
                if (reactionAddEvent.getGuild().getId().equals(serverId) && reactionAddEvent.getChannel().getName().equals("verify")) {
                    debounce(() -> {
                        Member member = reactionAddEvent.getMember();
                        if (hasRole(member, "Admin")) {
                            return;
                        }

                        reactionAddEvent.getReaction().removeReaction(reactionAddEvent.getUser()).queue();
                        boolean approve = reactionAddEvent.getReactionEmote().getName().equals("👍");
                        Guild guild = reactionAddEvent.getGuild();
                        if (approve) {
                            guild.addRoleToMember(member, getRole(guild, "Member")).queue();
                        } else {
                            guild.kick(member, "Verification failed").queue();
                        }
                    });
                }
            }
        });
    }

    private void handleWikiSearch(GuildMessageReceivedEvent receivedEvent) {
        debounce(() -> {
            String search = receivedEvent.getMessage().getContentRaw()
                    .replace("/wiki ", "");
            if (search.isEmpty()) {
                receivedEvent.getChannel().sendMessage("Invalid command: /wiki <search>").queue();
            } else {
                final String uri = "https://wiki.mineclick.net/graphql";

                HttpHeaders headers = new HttpHeaders();
                headers.setContentType(MediaType.APPLICATION_JSON);
                headers.set("Authorization", "Bearer " + wikiToken);

                JsonObject jsonObject = new JsonObject();
                jsonObject.add("query", new JsonPrimitive("query { pages { search(query: \"" + search + "\") { results { path, locale }}}}"));
                HttpEntity<String> request = new HttpEntity<>(jsonObject.toString(), headers);
                RestTemplate restTemplate = new RestTemplate();
                WikiResponse response = restTemplate.postForObject(uri, request, WikiResponse.class);
                if (response != null) {
                    List<String> results = new ArrayList<>();
                    List<WikiResponse.Result> resultList = response.getData().getPages().getSearch().getResults();
                    for (WikiResponse.Result result : resultList) {
                        if (result.getLocale().equals("en")) {
                            if (results.isEmpty()) {
                                results.add(result.getPath());
                            } else if (result.getPath().contains(search)) {
                                results.add(result.getPath());
                            }
                        }
                    }
                    if (!results.isEmpty()) {
                        String message = results.stream().reduce("", (acc, path) -> acc + "\nhttps://wiki.mineclick.net/en/" + path);
                        receivedEvent.getChannel().sendMessage(message).queue();
                        return;
                    }
                }

                receivedEvent.getChannel().sendMessage("Could not find anything for \"" + search + "\" in the wiki").queue();
            }
        });
    }

    private void handleMuteCommand(GuildMessageReceivedEvent receivedEvent) {
        debounce(() -> {
            Member member = receivedEvent.getMember();
            if (member != null && hasRole(member, "Staff")) {
                String[] args = receivedEvent.getMessage().getContentRaw()
                        .replace("/mute ", "").split(" ");
                if (args.length != 2) {
                    receivedEvent.getChannel().sendMessage("Invalid command: /mute <name> <minutes>").queue();
                } else {
                    receivedEvent.getChannel().sendMessage(":hourglass_flowing_sand: Hang on...").queue(loadingMessage -> {
                        String name = args[0];
                        int minutes = Integer.parseInt(args[1]);

                        usersService.getUuidFromName(name).subscribe(optional -> {
                            if (optional.isEmpty()) {
                                loadingMessage.delete().queue();
                                receivedEvent.getChannel().sendMessage("No username \"" + name + "\" found").queue();
                            } else {
                                UUID uuid = optional.get();
                                OffenceData offenceData = new OffenceData();
                                offenceData.setType(OffenceType.OTHER);
                                offenceData.setPunishment(PunishmentType.MUTE);
                                offenceData.setDurationMinutes(minutes);
                                offenceData.setPunishedBy(member.getEffectiveName());
                                offenceData.setPunishedOn(Instant.now().toString());

                                OffencesHandler handler = new OffencesHandler();
                                handler.setUuid(uuid);
                                handler.setOffenceData(offenceData);
                                handler.send(Action.POST);
                                handler.setResponseConsumer(message -> {
                                    String messageToAdmin = name + " was muted for " + minutes + " minutes";
                                    if (message == null || !message.isOk()) {
                                        Document document = usersService.getUserDocument(uuid);
                                        if (document != null) {
                                            List<OffenceData> offences = new ArrayList<>();
                                            offences.add(offenceData);
                                            if (document.containsKey("offences")) {
                                                offences.addAll(document.get("offences", List.class));
                                            }

                                            Update update = Update.update("offences", offences);
                                            UpdateResult result = mongoTemplate.updateFirst(Query.query(Criteria.where("_id").is(uuid.toString())), update, Constants.PLAYERS_DB);
                                            if (result.getModifiedCount() > 0) {
                                                messageToAdmin += " (offline)";
                                            } else {
                                                messageToAdmin = "Could not update the offline player";
                                            }
                                        }
                                    }

                                    loadingMessage.delete().queue();
                                    receivedEvent.getChannel().sendMessage(messageToAdmin).queue();
                                });
                            }
                        });
                    });
                }
            }
        });
    }

    private void handleHeyMessage(GuildMessageReceivedEvent receivedEvent) {
        receivedEvent.getMessage().addReaction("U+1F44B").queue();

        debounce(() -> redisTemplate.hasKey("discord.repliedHey").subscribe(hasKey -> { // check if the value was already set
            if (!hasKey) {
                String[] args = receivedEvent.getMessage().getContentRaw().toLowerCase().split(" ");
                long number = 0;
                try {
                    number = Long.parseLong(args[1]);
                } catch (Exception ignored) {
                }
                RandomBotMessages.get(number).subscribe(fact -> {
                    receivedEvent.getChannel().sendMessage("Hey!").queue();
                    receivedEvent.getChannel().sendMessage(fact).queue();
                });

                redisTemplate.opsForValue().set("discord.repliedHey", "").subscribe();
                redisTemplate.expire("discord.repliedHey", Duration.ofMinutes(10)).subscribe();
            }
        }));
    }

    private void debounce(Runnable runnable) {
        if (Messenger.getI().isCurrentNode()) {
            runnable.run();
        }
    }

    /**
     * Message a user on discord (if found)
     *
     * @param userId  The user id
     * @param message The message
     */
    public void msgUser(String userId, String message) {
        if (userId == null || discordApi == null) return;

        discordApi.retrieveUserById(userId)
                .flatMap(User::openPrivateChannel)
                .flatMap(channel -> channel.sendMessage(message))
                .queue();
    }

    /**
     * Retrieve a Discord user
     *
     * @param discordId Their discord id
     * @param consumer  Consumer
     */
    public void getUser(String discordId, Consumer<User> consumer) {
        if (discordApi == null) return;

        discordApi.retrieveUserById(discordId).queue(consumer);
    }

    public void notifyUser(String userId, String message) {
        msgUser(userId, ">>> " + message + "\n \n_Type_ **/unlink** _to unlink your Discord account " +
                "and stop receiving any future notifications._");
    }

    /**
     * Send a message to a discord channel (if found)
     *
     * @param channelName The channel name
     * @param message     The message
     */
    public void msgChannel(String channelName, String message) {
        TextChannel channel = getChannelByName(channelName);
        if (channel != null) {
            channel.sendMessage(message).queue();
        }
    }

    /**
     * Update this discord user if linked.
     * Set appropriate roles and nickname
     *
     * @param playerId The user to update
     */
    public void updateUser(PlayerId playerId) {
        if (playerId.getDiscordId() == null || playerId.getDiscordId().isEmpty() || discordApi == null) return;

        Guild guild = discordApi.getGuildById(serverId);
        if (guild != null) {
            Set<Role> roles = new HashSet<>();

            Role inGameRole = getRole(guild, "In game");
            Role premiumRole = getRole(guild, "Premium");
            if (playerId.isOnline()) {
                roles.add(inGameRole);
            }

            if (!playerId.isDev()) {
                if (playerId.isStaff()) {
                    roles.add(getRole(guild, "Staff"));
                } else if (playerId.isPremium()) {
                    roles.add(premiumRole);
                }
                roles.add(getRole(guild, "Verified"));
            }

            guild.retrieveMemberById(playerId.getDiscordId())
                    .queue(member -> {
                        roles.forEach(role -> {
                            if (role != null) {
                                guild.addRoleToMember(member, role).queue();
                            }
                        });

                        if (!playerId.isOnline()) {
                            guild.removeRoleFromMember(member, inGameRole).queue();
                        }
                        if (!playerId.isPremium()) {
                            guild.removeRoleFromMember(member, premiumRole).queue();
                        }

                        try {
                            guild.modifyNickname(member, playerId.getName()).queue();
                        } catch (HierarchyException ignored) {
                        }
                    });
        }
    }

    /**
     * Clear premium, verified and in game roles from the given discord user
     *
     * @param discordId The discord user id
     */
    public void clearRoles(String discordId) {
        if (discordApi == null) return;

        Guild guild = discordApi.getGuildById(serverId);
        if (guild != null) {
            guild.retrieveMemberById(discordId).queue(member -> {
                Set<Role> roles = new HashSet<>();
                roles.add(getRole(guild, "Premium"));
                roles.add(getRole(guild, "Verified"));
                roles.add(getRole(guild, "In game"));

                roles.forEach(role -> {
                    if (role != null) {
                        guild.removeRoleFromMember(member, role).queue();
                    }
                });
            });
        }
    }

    public Criteria notificationBaseCriteria(String setting) {
        return new Criteria()
                .andOperator(
                        Criteria.where("discordId").ne(null),
                        Criteria.where("playerSettings." + setting).is(true),
                        new Criteria().orOperator(
                                Criteria.where("online").is(false),
                                Criteria.where("activityData.afk").is(true).and("playerSettings.discordAfk").is(true)
                        )
                );
    }

    /**
     * Notify this player's friends that the player has joined the game.
     * Only friends with the notification setting on and a discord id
     * will receive the message.
     *
     * @param player The player that joined
     */
    public void notifyFriendJoin(PlayerId player) {
        String uuid = player.getUuid();

        Query query = Query.query(notificationBaseCriteria("discordFriends").and("friendsData.friends").is(uuid));
        List<PlayerId> friends = mongoTemplate.find(query, PlayerId.class, Constants.PLAYERS_DB);

        for (PlayerId friend : friends) {
            notifyUser(friend.getDiscordId(), "Your friend **" + player.getName() + "** joined mineclick.net");
        }
    }

    /**
     * Get the Discord role
     *
     * @param name Role name (case insensitive)
     * @return Matching role or null
     */
    public Role getRole(String name) {
        if (discordApi == null) return null;

        Guild guild = discordApi.getGuildById(serverId);
        return guild == null ? null : getRole(guild, name);
    }

    /**
     * Get the Discord role
     *
     * @param guild The guild
     * @param name  Role name (case insensitive)
     * @return Matching role or null
     */
    public Role getRole(Guild guild, String name) {
        return guild.getRolesByName(name, true).stream().findFirst().orElse(null);
    }

    /**
     * Find a text channel by its name
     *
     * @param name The channel's name
     * @return The first matching text channel or null
     */
    public TextChannel getChannelByName(String name) {
        if (discordApi == null) return null;

        Guild guild = discordApi.getGuildById(serverId);
        if (guild != null) {
            return guild.getTextChannelsByName(name, true)
                    .stream()
                    .findFirst()
                    .orElse(null);
        }

        return null;
    }

    /**
     * Find an emote by its name
     *
     * @param name The emote's name
     * @return The first matching emote or null
     */
    public Emote getEmoteByName(String name) {
        if (discordApi == null) return null;

        Guild guild = discordApi.getGuildById(serverId);
        if (guild != null) {
            return guild.getEmotesByName(name, true)
                    .stream()
                    .findFirst()
                    .orElse(null);
        }

        return null;
    }

    /**
     * Check if the Discord member has this role by its name
     *
     * @param member   The member
     * @param roleName The role name. Case-sensitive
     * @return True if member has this role
     */
    public boolean hasRole(Member member, String roleName) {
        return member.getRoles().stream().anyMatch(role -> role.getName().equals(roleName));
    }

    /**
     * Check for refilled daily reward chests and vaults
     */
    @Scheduled(fixedRate = 60000)
    private void notifyCheck() {
        notifyBySetting("discordReward", "Your mineclick.net **daily reward chest** is refilled");
        notifyBySetting("discordVaults", "Your mineclick.net **vaults** are filled up");
    }

    private void notifyBySetting(String setting, String message) {
        Criteria criteria = notificationBaseCriteria(setting).and("notify." + setting).lte(System.currentTimeMillis());
        Update update = new Update().unset("notify." + setting);

        PlayerId playerId = mongoTemplate.findAndModify(Query.query(criteria), update, PlayerId.class, Constants.PLAYERS_DB);
        if (playerId != null) {
            notifyUser(playerId.getDiscordId(), message);

            DiscordNotifyHandler handler = new DiscordNotifyHandler(playerId.getUuid(), setting);
            handler.send(Action.DELETE);
        }
    }
}

package net.mineclick.ender.service;

import com.google.common.collect.ImmutableMap;
import net.mineclick.core.messenger.Action;
import net.mineclick.core.messenger.Message;
import net.mineclick.core.messenger.Response;
import net.mineclick.ender.Ender;
import net.mineclick.ender.component.JwtTokenUtil;
import net.mineclick.ender.component.MongoUpdater;
import net.mineclick.ender.messenger.MessageHandler;
import net.mineclick.ender.model.PlayerId;
import net.mineclick.ender.model.WebUser;
import net.mineclick.ender.type.Rank;
import net.mineclick.ender.utils.Constants;
import org.bson.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.index.Index;
import org.springframework.data.mongodb.core.index.IndexDefinition;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.data.redis.core.ReactiveRedisTemplate;
import org.springframework.data.redis.core.ReactiveValueOperations;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.Duration;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

@Service
public class UsersService implements UserDetailsService {
    private static final Random random = new Random();

    @Autowired
    private MongoUpdater mongoUpdater;
    @Autowired
    private MongoTemplate mongoTemplate;
    @Autowired
    private ReactiveRedisTemplate<String, String> redisTemplate;
    @Autowired
    private JwtTokenUtil jwtTokenUtil;
    @Autowired
    private PasswordEncoder passwordEncoder;

    /**
     * Get a <b>registered</b> user details by their username or email
     *
     * @param username The username or email
     * @return The UserDetails or null
     * @throws UsernameNotFoundException If user not found or not registered
     */
    public UserDetails loadUserByUsername(String username) {
        WebUser user = getWebUser(username);
        if (user != null) {
            // Registered means the password was set
            if (user.getRegistrationStatus() != null && user.getRegistrationStatus().equals(WebUser.Status.REGISTERED)) {
                Rank rank = Rank.valueOf(user.getRank());
                return User.builder()
                        .username(user.getName())
                        .password(user.getPassword())
                        .roles(rank.getSubRanks())
                        .accountLocked(user.isWebAccountLocked()) // TODO Might mean they need to change their password to unlock
                        .disabled(user.isWebAccountDisabled()) // TODO When demoted
                        .build();
            }
        }

        throw new UsernameNotFoundException("Invalid username or password");
    }

    /**
     * Returns a web user regardless if they were registered or not
     *
     * @param username The username or email of the user
     * @return The web user or null
     */
    public WebUser getWebUser(String username) {
        return getWebUserByField(ImmutableMap.of("name", username, "email", username));
    }

    /**
     * Returns a web user queried by the specified field
     *
     * @param field The field name
     * @param value The value of the field
     * @return The web user or null
     */
    public WebUser getWebUserByField(String field, String value) {
        return getWebUserByField(ImmutableMap.of(field, value));
    }

    /**
     * Returns a web user queried by the specified fields
     *
     * @param fields A map of fields to query by ("or" list)
     * @return The web user or null
     */
    public WebUser getWebUserByField(Map<String, String> fields) {
        Criteria criteria = new Criteria();
        criteria.orOperator(fields.entrySet().stream()
                .map(entry -> Criteria.where(entry.getKey()).is(entry.getValue()))
                .toArray(Criteria[]::new));
        Query query = new Query(criteria);

        return mongoTemplate.findOne(query, WebUser.class, Constants.PLAYERS_DB);
    }

    /**
     * Search for a user either by their name or uuid
     *
     * @param search Name or uuid
     * @return The entire JSON of the user
     */
    public Document searchFullUser(String search) {
        Document document = mongoTemplate.getCollection(Constants.PLAYERS_DB)
                .find(Document.parse("{$or: [{_id: \"" + search + "\"}, {name: /^" + search + "$/i}]}"))
                .first();

        // Remove sensitive information
        if (document != null) {
            document.remove("password");
            document.remove("otpSignature");
            document.remove("passwordResetStatus");
            document.remove("discordState");
        }

        return document;
    }

    public void updateWebUser(WebUser user) {
        Query query = new Query(Criteria.where("_id").is(user.getUuid()));
        Update update = mongoUpdater.getUpdateFor(user);
        mongoTemplate.upsert(query, update, Constants.PLAYERS_DB);
    }

    public void updateUserField(UUID uuid, Update update) {
        Query query = new Query(Criteria.where("_id").is(uuid.toString()));
        mongoTemplate.upsert(query, update, Constants.PLAYERS_DB);
    }

    public Document getUserDocument(UUID uuid) {
        return mongoTemplate.getCollection(Constants.PLAYERS_DB)
                .find(Document.parse("{_id: \"" + uuid + "\"}"))
                .first();
    }

    public void sendOtpCode(WebUser user) {
        String code = getOTPCode(user.getName());

        sendGameMessage(user.getUuid(), "Your OTP code is: &6" + code);
    }

    /**
     * Creates or retrieves an already created one time password code for this username.
     * Valid for 5 minutes
     *
     * @param username The username
     * @return The OTP code
     */
    public String getOTPCode(String username) {
        String key = otpCodeKey(username);

        ReactiveValueOperations<String, String> ops = redisTemplate.opsForValue();
        String code = ops.get(key).block();
        if (code == null) {
            code = String.format("%06d", random.nextInt(999999));
            ops.set(key, code).block();
            redisTemplate.expire(key, Duration.ofMinutes(5)).block();
        }

        Ender.logger.info("OTP: " + username + " " + code);
        return code;
    }

    /**
     * Validate the OTP code
     *
     * @param username The username
     * @param code     The code
     * @return True if valid
     */
    public boolean validateOTPCode(String username, String code) {
        String c = redisTemplate.opsForValue().get(otpCodeKey(username)).block();

        if (code.equals(c)) {
            redisTemplate.delete(otpCodeKey(username)).subscribe();
            return true;
        }
        return false;
    }

    private String otpCodeKey(String username) {
        return "ender.otp:" + username;
    }

    /**
     * Saves the auth token with the token's ttl
     *
     * @param username The username of the user
     * @param token    The token
     * @param ttl      Time to live Duration
     */
    public void saveToken(String username, String token, Duration ttl) {
        redisTemplate.opsForValue().set("ender.token:" + username, token)
                .subscribe(aBoolean -> redisTemplate.expire("ender.token:" + username, ttl).subscribe());
    }

    /**
     * Invalidate the currently saved token for this user
     *
     * @param username The username
     */
    public void invalidateToken(String username) {
        redisTemplate.opsForValue().delete("ender.token:" + username).subscribe();
    }

    /**
     * Check if the token is still valid
     *
     * @param username The username
     * @param token    The token
     * @return Token if the token has not been expired either from a set ttl or manually
     */
    public boolean isTokenValid(String username, String token) {
        return token.equals(redisTemplate.opsForValue().get("ender.token:" + username).block());
    }

    /**
     * Generate and save a token for this user
     *
     * @param username The username
     * @return The generated token
     */
    public String generateToken(String username) {
        String token = jwtTokenUtil.generateToken(username);
        saveToken(username, token, Duration.ofDays(1));

        return token;
    }

    // TODO introduce some caching for player's uuids/name

    /**
     * Query the db to resolve the player's name by uuid
     *
     * @param uuid The uuid
     * @return Mono containing the player's name. The mono will resolve to empty if no player/name is found
     */
    public Mono<Optional<String>> getNameFromUuid(String uuid) {
        return Mono.fromSupplier(() -> {
            Query query = Query.query(Criteria.where("_id").is(uuid));
            query.fields().include("name");
            PlayerId playerId = mongoTemplate.findOne(query, PlayerId.class, Constants.PLAYERS_DB);

            return playerId == null || playerId.getName() == null ? Optional.empty() : Optional.of(playerId.getName());
        });
    }

    /**
     * Query the db to resolve multiple players' names by uuids
     *
     * @param uuids The uuids
     * @return Mono containing the mapping of player's name to their uuid
     */
    public Mono<Map<String, String>> getNameFromUuid(Set<String> uuids) {
        return Flux.fromStream(() -> {
            Query query = Query.query(Criteria.where("_id").in(uuids));
            query.fields().include("name");
            return mongoTemplate.find(query, PlayerId.class, Constants.PLAYERS_DB).stream();
        }).collectMap(PlayerId::getUuid, PlayerId::getName);
    }

    /**
     * Query the db to retrieve the player's name by uuid
     *
     * @param name The name. Case-insensitive
     * @return Mono containing the player's uuid. The mono will resolve to empty if no player/uuid is found
     */
    public Mono<Optional<UUID>> getUuidFromName(String name) {
        return Mono.fromSupplier(() -> {
            Query query = Query.query(Criteria.where("name").regex(name, "i"));
            query.fields().include("_id");
            PlayerId playerId = mongoTemplate.findOne(query, PlayerId.class, Constants.PLAYERS_DB);

            return playerId == null || playerId.getUuid() == null ? Optional.empty() : Optional.of(UUID.fromString(playerId.getUuid()));
        });
    }

    /**
     * Check if the raw password matches that of the encoded password
     *
     * @param rawPassword The raw password
     * @param encoded     The encoded password
     * @return True if the passwords match
     */
    public boolean validatePassword(String rawPassword, String encoded) {
        return passwordEncoder.matches(rawPassword, encoded);
    }

    /**
     * Get textures for the given uuids collection.
     * No guarantee the same number of uuids will be resolved
     *
     * @param uuids The uuid
     * @return Uuid/texture mapping
     */
    public Map<UUID, String> getTextures(Set<UUID> uuids) {
        Set<String> set = uuids.stream().map(UUID::toString).collect(Collectors.toSet());
        Query query = Query.query(Criteria.where("_id").in(set));
        query.fields().include("texture");

        List<PlayerId> playerIds = mongoTemplate.find(query, PlayerId.class, Constants.PLAYERS_DB);
        return playerIds.stream()
                .filter(playerId -> playerId.getTexture() != null)
                .collect(Collectors.toMap(playerId -> UUID.fromString(playerId.getUuid()), PlayerId::getTexture));
    }

    /**
     * Send a message to a player in game (if online)
     *
     * @param uuid    Player uuid
     * @param message The message
     * @return True is sent successfully
     */
    public boolean sendGameMessage(String uuid, String message) {
        MessageHandler handler = new MessageHandler();
        handler.setUuid(uuid);
        handler.setMessage(message);
        handler.send(Action.POST);

        handler.blockResponse();
        Message responseMessage = handler.getResponseMessage();
        return responseMessage != null && responseMessage.getResponse().equals(Response.OK);
    }

    /**
     * Create any necessary mongo indexes
     */
    public void initMongo() {
        IndexDefinition indexDefinition = new Index("date", Sort.Direction.DESC).expire(5, TimeUnit.DAYS);
        mongoTemplate.indexOps(Constants.PLAYERS_SNAPSHOT_DB)
                .ensureIndex(indexDefinition);
    }
}

package net.mineclick.ender.service;

import net.mineclick.ender.component.MongoUpdater;
import net.mineclick.ender.model.AppealCase;
import net.mineclick.ender.model.ChatData;
import net.mineclick.ender.model.OffenceData;
import net.mineclick.ender.type.PunishmentType;
import net.mineclick.ender.utils.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.List;

@Service
public class AppealsService {
    @Autowired
    private MongoTemplate mongoTemplate;
    @Autowired
    private MongoUpdater mongoUpdater;
    @Autowired
    private DiscordService discordService;

    @Value("${discord.staffChannelName}")
    private String staffChannelName;

    /**
     * @return A list of active (appealed by player) appeal cases
     */
    public List<AppealCase> getAppeals() {
        Criteria criteria = Criteria.where("offences").elemMatch(
                Criteria.where("punishment").is(PunishmentType.BAN.toString())
                        .and("pardonedOn").is(null)
                        .and("deniedOn").is(null)
                        .and("appealedOn").ne(null)
        );
        Query query = Query.query(criteria);

        return mongoTemplate.find(query, AppealCase.class, Constants.PLAYERS_DB);
    }

    /**
     * Find a ban appeal case (either appealed or not) for the given username
     *
     * @param username The username
     * @return The appeal case
     */
    public AppealCase getAppeal(String username) {
        Criteria criteria = Criteria.where("name").is(username)
                .and("offences").elemMatch(Criteria.where("punishment").is(PunishmentType.BAN.toString()));
        Query query = Query.query(criteria);
        return mongoTemplate.findOne(query, AppealCase.class, Constants.PLAYERS_DB);
    }

    /**
     * Post an appeal for a ban with the appeal message
     *
     * @param username The username
     * @param message  The appeal message
     * @return The updated case
     */
    public AppealCase postAppeal(String username, String message) {
        AppealCase appeal = getAppeal(username);
        if (appeal == null) return null;

        if (appeal.getOffences() != null && !appeal.getOffences().isEmpty()) {
            OffenceData offenceData = appeal.getOffences().get(0);
            offenceData.setAppealedOn(Instant.now().toString());
            offenceData.setAppealMessage(message);
        }

        Update update = mongoUpdater.getUpdateFor(appeal);
        mongoTemplate.upsert(Query.query(Criteria.where("_id").is(appeal.getUuid())), update, Constants.PLAYERS_DB);

        discordService.msgChannel(staffChannelName, username + " submitted a ban appeal: https://ender.mineclick.net/appeals");

        return appeal;
    }

    /**
     * Get chat history for a given player
     *
     * @param username The player
     * @return List of chat messages
     */
    public List<String> getChatHistory(String username) {
        ChatData.Player player = mongoTemplate.findOne(Query.query(Criteria.where("name").is(username)), ChatData.Player.class, Constants.PLAYERS_DB);

        if (player != null && player.getChatData() != null) {
            return player.getChatData().getHistory();
        }
        return null;
    }

    public AppealCase postAppealStatus(User user, String username, OffenceData.Status status) {
        AppealCase appeal = getAppeal(username);
        if (appeal == null) return null;

        if (appeal.getOffences() != null && !appeal.getOffences().isEmpty()) {
            OffenceData offenceData = appeal.getOffences().get(0);
            if (status.equals(OffenceData.Status.PARDONED)) {
                offenceData.setPardonedOn(Instant.now().toString());
                offenceData.setPardonedBy(user.getUsername());
            } else if (status.equals(OffenceData.Status.DENIED)) {
                offenceData.setDeniedOn(Instant.now().toString());
                offenceData.setDeniedBy(user.getUsername());
            } else {
                offenceData.setDeniedOn(null);
                offenceData.setDeniedBy(null);
                offenceData.setPardonedOn(null);
                offenceData.setPardonedBy(null);
            }
        }
        discordService.msgChannel(staffChannelName, user.getUsername() + " " + (status.equals(OffenceData.Status.BANNED) ? "undone" : status.toString()) + " " + username + "'s appeal");

        Update update = mongoUpdater.getUpdateFor(appeal);
        mongoTemplate.upsert(Query.query(Criteria.where("_id").is(appeal.getUuid())), update, Constants.PLAYERS_DB);

        return appeal;
    }
}

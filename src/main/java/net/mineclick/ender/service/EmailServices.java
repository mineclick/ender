package net.mineclick.ender.service;

import com.sendgrid.Method;
import com.sendgrid.Request;
import com.sendgrid.SendGridAPI;
import com.sendgrid.helpers.mail.Mail;
import com.sendgrid.helpers.mail.objects.Email;
import com.sendgrid.helpers.mail.objects.Personalization;
import lombok.SneakyThrows;
import net.mineclick.ender.type.EmailType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public class EmailServices {
    @Autowired
    private SendGridAPI sendGrid;

    /**
     * Send an email using a template
     *
     * @param type         The email type (consequently the template id)
     * @param to           The recipient email
     * @param name         The recipient name (optional)
     * @param templateData Template data (optional)
     */
    public void send(EmailType type, String to, String name, Map<String, Object> templateData) {
        sendEmail(buildMailFromTemplate(to, name, type.getFromEmail(), type.getFromName(), type.getTemplateId(), templateData));
    }

    /**
     * Build a Mail object using a template
     *
     * @param to            The recipient email
     * @param name          The recipient name (optional)
     * @param from          From email
     * @param fromName      From name
     * @param templateToken Template token
     * @param substitutions Template substitutions
     * @return The mail object
     */
    public Mail buildMailFromTemplate(String to, String name, String from, String fromName, String templateToken, Map<String, Object> substitutions) {
        Email toEmail = new Email(to);
        if (name != null) {
            toEmail.setName(name);
        }

        Mail mail = new Mail();
        mail.setFrom(new Email(from, fromName));
        mail.setTemplateId(templateToken);

        Personalization personalization = new Personalization();
        personalization.addTo(toEmail);
        if (substitutions != null) {
            substitutions.forEach(personalization::addDynamicTemplateData);
        }

        mail.addPersonalization(personalization);
        return mail;
    }

    @SneakyThrows
    private void sendEmail(Mail mail) {
        Request request = new Request();
        request.setMethod(Method.POST);
        request.setEndpoint("mail/send");
        request.setBody(mail.build());

        sendGrid.api(request);
    }
}

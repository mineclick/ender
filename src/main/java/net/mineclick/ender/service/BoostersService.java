package net.mineclick.ender.service;

import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.MessageBuilder;
import net.dv8tion.jda.api.entities.Emote;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.TextChannel;
import net.mineclick.core.messenger.Action;
import net.mineclick.ender.messenger.BoostersHandler;
import net.mineclick.ender.model.PlayerId;
import net.mineclick.ender.model.ServerModel;
import net.mineclick.ender.model.booster.ActiveBooster;
import net.mineclick.ender.model.booster.BoosterHistory;
import net.mineclick.ender.type.BoosterType;
import net.mineclick.ender.utils.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.time.temporal.ChronoUnit;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class BoostersService {
    @Autowired
    private MongoTemplate mongoTemplate;
    @Autowired
    private DiscordService discordService;
    @Autowired
    private ServersService serversService;

    @Value("${discord.boostersChannelName}")
    private String boostersChannelName;

    /**
     * Get a set of currently active boosters
     *
     * @return The active boosters
     */
    public List<ActiveBooster> getActiveBoosters() {
        Query query = Query.query(Criteria.where("expiresAt").gt(new Date()));
        List<BoosterHistory> history = mongoTemplate.find(query, BoosterHistory.class, Constants.BOOSTERS_DB);

        return history.stream()
                .collect(Collectors.groupingBy(BoosterHistory::getType))
                .entrySet().stream()
                .map(entry -> {
                    List<String> players = entry.getValue().stream()
                            .sorted(Comparator.comparing(BoosterHistory::getExpiresAt).reversed())
                            .map(BoosterHistory::getPlayer)
                            .distinct()
                            .limit(5)
                            .collect(Collectors.toList());
                    Date expiresAt = entry.getValue().stream()
                            .map(BoosterHistory::getExpiresAt)
                            .max(Comparator.naturalOrder()).orElse(new Date());

                    ActiveBooster booster = new ActiveBooster();
                    booster.setType(entry.getKey());
                    booster.setPlayers(players);
                    booster.setExpiresAt(expiresAt);
                    return booster;
                })
                .sorted(Comparator.comparing(ActiveBooster::getExpiresAt).reversed())
                .collect(Collectors.toList());
    }

    /**
     * Activate a booster
     *
     * @param booster The booster to be activated
     */
    public void activateBooster(ActiveBooster booster) {
        Criteria criteria = new Criteria();
        criteria.andOperator(Criteria.where("expiresAt").gt(new Date()), Criteria.where("type").is(booster.getType()));
        Query query = Query.query(criteria);
        List<BoosterHistory> history = mongoTemplate.find(query, BoosterHistory.class, Constants.BOOSTERS_DB);

        Date expiresAt = history.stream()
                .map(BoosterHistory::getExpiresAt)
                .max(Comparator.naturalOrder()).orElse(new Date());
        expiresAt = Date.from(expiresAt.toInstant().plus(booster.getDuration(), ChronoUnit.MINUTES));

        BoosterHistory newHistory = new BoosterHistory();
        newHistory.setType(booster.getType());
        newHistory.setActivatedAt(new Date());
        newHistory.setExpiresAt(expiresAt);
        if (!booster.getPlayers().isEmpty()) {
            newHistory.setPlayer(booster.getPlayers().get(0));
        }

        mongoTemplate.insert(newHistory, Constants.BOOSTERS_DB);

        // Notify servers of the update
        BoostersHandler handler = new BoostersHandler();
        handler.setBoosters(getActiveBoosters());
        handler.send(Action.UPDATE);

        // Notify premium players
        Criteria boosterNotifyCriteria = discordService.notificationBaseCriteria("discordBooster");
        List<PlayerId> players = mongoTemplate.find(Query.query(boosterNotifyCriteria), PlayerId.class, Constants.PLAYERS_DB);
        for (PlayerId playerId : players) {
            discordService.notifyUser(playerId.getDiscordId(), "The **" + booster.getType().getName()
                    + "** was activated by " + booster.getPlayers().get(0));
        }
    }

    @Scheduled(fixedRate = 15000)
    private void updateBoostersDiscord() {
        TextChannel channel = discordService.getChannelByName(boostersChannelName);
        if (channel == null) return;

        channel.getHistoryFromBeginning(1).queue(history -> {
            if (history.isEmpty()) {
                channel.sendMessage("Loading...").queue(this::updateDiscordMessage);
            } else {
                updateDiscordMessage(history.getRetrievedHistory().get(0));
            }
        });
    }

    private void updateDiscordMessage(Message message) {
        List<ActiveBooster> boosters = getActiveBoosters();

        EmbedBuilder embedBuilder = new EmbedBuilder();
        if (boosters.isEmpty()) {
            embedBuilder.setColor(13632027);
            embedBuilder.addField("_ _", "_ _", false);
        } else {
            embedBuilder.setColor(8311585);
            for (ActiveBooster booster : boosters) {
                BoosterType type = booster.getType();
                int minutes = (int) ((booster.getExpiresAt().getTime() - System.currentTimeMillis()) / 60000);
                String players = String.join(", ", booster.getPlayers());

                String emoteString = "";
                Emote emote = discordService.getEmoteByName(type.getEmote());
                if (emote != null) {
                    emoteString = emote.getAsMention() + " ";
                }

                embedBuilder.addField(emoteString + type.getName(), "Activated by: " + players, true);
                embedBuilder.addField(minutes + " minute" + (minutes == 1 ? "s" : "") + " left", "_ _", true);
                embedBuilder.addField("_ _", "_ _", false);
            }
        }

        int playersCount = serversService.getServers().stream().mapToInt(ServerModel::getPlayers).sum();

        embedBuilder.addField("_ _", "Boosters apply to all online players. Boosters can be purchased at [the store](http://store.mineclick.net)\n"+playersCount + " players online", false);
        embedBuilder.setAuthor(
                (boosters.isEmpty() ? "No" : boosters.size()) + " currently active booster" + (boosters.size() == 1 ? "" : "s"),
                "http://store.mineclick.net",
                null
        );

        Message newMessage = new MessageBuilder()
                .append("**MineClick Boosters**")
                .setEmbed(embedBuilder.build())
                .build();
        message.editMessage(newMessage).queue();
    }
}

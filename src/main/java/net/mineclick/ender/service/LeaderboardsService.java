package net.mineclick.ender.service;

import net.mineclick.ender.model.LeaderboardData;
import net.mineclick.ender.model.Statistic;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Range;
import org.springframework.data.redis.core.ReactiveRedisTemplate;
import org.springframework.data.redis.core.ReactiveZSetOperations;
import org.springframework.data.redis.core.ZSetOperations;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class LeaderboardsService {
    @Autowired
    private ReactiveRedisTemplate<String, String> redisTemplate;
    @Autowired
    private UsersService usersService;


    /**
     * Retrieve the leaderboard for a given statistic key and the top players count.<br><br>
     * <b>This method can be quite expensive:</b> since the statistics are saved by player's uuid,
     * they would need to be mapped to the player's name which involves a MongoDB query (thankfully just one).<br>
     * Caching of the above mapping is WIP.
     *
     * @param key   The statistic key
     * @param count The number of top players to retrieve
     * @return A Mono containing the retrieved leaderboard
     */
    // This would be a great interview question: can you understand what's going on here?
    public Mono<LeaderboardData> getLeaderboard(String key, int count) {
        ReactiveZSetOperations<String, String> ops = redisTemplate.opsForZSet();

        Range<Long> range = Range.from(Range.Bound.inclusive(0L)).to(Range.Bound.exclusive((long) count));
        return ops
                .reverseRangeWithScores("server.statistics:" + key, range)
                .collectList()
                .flatMap(tuples -> {
                    Set<String> uuids = tuples.stream().map(ZSetOperations.TypedTuple::getValue).collect(Collectors.toSet());

                    LeaderboardData leaderboard = new LeaderboardData(key, count);
                    Map<String, Statistic> statistics = new HashMap<>();
                    return usersService.getNameFromUuid(uuids).flatMap(map -> {
                        for (int i = 0; i < tuples.size(); i++) {
                            ZSetOperations.TypedTuple<String> tuple = tuples.get(i);
                            if (tuple.getScore() != null && tuple.getValue() != null) {
                                String name = map.get(tuple.getValue());
                                if (name != null) {
                                    Statistic statistic = new Statistic();
                                    statistic.setRank(i + 1);
                                    statistic.setScore(tuple.getScore());

                                    statistics.put(name, statistic);
                                }
                            }
                        }

                        leaderboard.setStatistics(statistics);
                        return Mono.just(leaderboard);
                    });
                });
    }
}

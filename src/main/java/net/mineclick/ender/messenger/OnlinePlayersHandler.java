package net.mineclick.ender.messenger;

import com.mongodb.client.result.UpdateResult;
import lombok.Getter;
import net.mineclick.core.messenger.Action;
import net.mineclick.core.messenger.Message;
import net.mineclick.core.messenger.MessageName;
import net.mineclick.core.messenger.Response;
import net.mineclick.ender.component.StaticServiceProvider;
import net.mineclick.ender.model.PlayerId;
import net.mineclick.ender.utils.Constants;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@MessageName("onlinePlayers")
public class OnlinePlayersHandler extends Message {
    @Getter
    private List<PlayerId> players = new ArrayList<>();

    @Override
    public void onReceive() {
        wrap(action -> {
            MongoTemplate template = StaticServiceProvider.i().getMongoTemplate();
            switch (action) {
                case GET: {
                    Query query = new Query();
                    query.addCriteria(Criteria.where("online").is(true));
                    for (Field field : PlayerId.class.getDeclaredFields()) {
                        query.fields().include(field.getName());
                    }

                    players = template.find(query, PlayerId.class, Constants.PLAYERS_DB);
                    return Response.OK;
                }
                case POST:
                case DELETE: {
                    boolean online = action == Action.POST;

                    Query query = new Query();
                    query.addCriteria(Criteria.where("_id").in(players.stream()
                            .map(PlayerId::getUuid)
                            .collect(Collectors.toList())));
                    Update update = new Update();
                    update.set("online", online);
                    UpdateResult result = template.updateMulti(query, update, Constants.PLAYERS_DB);

                    // Update discord
                    players.forEach(playerId -> {
                        // bungee doesn't pass the entire player object so we need to do some queries
                        if (!online) {
                            PlayerId fullPlayer = template.findOne(Query.query(Criteria.where("_id").is(playerId.getUuid())), PlayerId.class, Constants.PLAYERS_DB);
                            if (fullPlayer != null) {
                                playerId = fullPlayer;
                            }
                        }

                        playerId.setOnline(online);
                        StaticServiceProvider.i().getDiscordService().updateUser(playerId);
                        if (online) {
                            StaticServiceProvider.i().getDiscordService().notifyFriendJoin(playerId);
                        }
                    });

                    return result.getModifiedCount() > 0 ? Response.OK : Response.NOT_FOUND;
                }
            }
            return null;
        });
    }
}

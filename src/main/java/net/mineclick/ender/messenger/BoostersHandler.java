package net.mineclick.ender.messenger;

import lombok.Getter;
import lombok.Setter;
import net.mineclick.core.messenger.Action;
import net.mineclick.core.messenger.Message;
import net.mineclick.core.messenger.MessageName;
import net.mineclick.core.messenger.Response;
import net.mineclick.ender.component.StaticServiceProvider;
import net.mineclick.ender.model.booster.ActiveBooster;

import java.util.List;

@Getter
@Setter
@MessageName("boosters")
public class BoostersHandler extends Message {
    private List<ActiveBooster> boosters;

    @Override
    public void onReceive() {
        wrap(action -> {
            if (action.equals(Action.GET)) {
                boosters = StaticServiceProvider.i().getBoostersService().getActiveBoosters();
                return Response.OK;
            }

            if (action.equals(Action.POST)) {
                if (boosters == null || boosters.isEmpty()) {
                    return Response.ERROR;
                }

                StaticServiceProvider.i().getBoostersService().activateBooster(boosters.iterator().next());
                return Response.OK;
            }

            return null;
        });
    }
}

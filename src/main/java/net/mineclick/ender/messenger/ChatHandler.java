package net.mineclick.ender.messenger;

import lombok.Setter;
import net.mineclick.core.messenger.Message;
import net.mineclick.core.messenger.MessageName;
import net.mineclick.ender.component.StaticServiceProvider;
import net.mineclick.ender.model.PlayerId;
import net.mineclick.ender.service.DiscordService;

@Setter
@MessageName("chat")
public class ChatHandler extends Message {
    private ChatType chatType;
    private PlayerId senderData;
    private String message;

    @Override
    public void onReceive() {
        if (chatType == null || senderData == null || message == null) return;

        boolean staffChat = chatType.equals(ChatType.STAFF);
        if (chatType.equals(ChatType.PUBLIC) || staffChat) {
            DiscordService discord = StaticServiceProvider.i().getDiscordService();
            discord.getChannelByName(discord.getGameChatChannelName())
                    .sendMessage((staffChat ? "**|S|**" : "") + "`" + senderData.getName() + "`: " + message).queue();
        }
    }

    public enum ChatType {
        PUBLIC, PRIVATE, BROADCAST, STAFF
    }
}

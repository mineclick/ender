package net.mineclick.ender.messenger;

import net.mineclick.core.messenger.Message;
import net.mineclick.core.messenger.MessageName;
import net.mineclick.ender.component.StaticServiceProvider;
import net.mineclick.ender.model.ServerModel;

@MessageName("servers")
public class ServersHandler extends Message {
    private ServerModel server;

    @Override
    public void onReceive() {
        StaticServiceProvider.i().getServersService().handle(server);
    }
}

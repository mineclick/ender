package net.mineclick.ender.messenger;

import net.mineclick.core.messenger.Message;
import net.mineclick.core.messenger.MessageName;
import net.mineclick.core.messenger.Response;
import net.mineclick.ender.component.StaticServiceProvider;
import net.mineclick.ender.model.Transaction;
import net.mineclick.ender.model.TransactionItem;
import net.mineclick.ender.type.EmailType;
import net.mineclick.ender.utils.Constants;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

@MessageName("transactions")
public class TransactionsHandler extends Message {
    private static final NumberFormat NUMBER_FORMAT = new DecimalFormat("#,###.##");
    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

    private String uuid;
    private String email;
    private String isProcessed;
    private String isCancelled;
    private String isSubscription; // TODO doesn't seem to be used?
    private boolean sendEmail;
    private List<Transaction> transactions;

    @Override
    public void onReceive() {
        wrap(action -> {
            switch (action) {
                case GET: {
                    Query query = new Query();

                    // Query by transaction ids
                    if (transactions != null) {
                        List<String> ids = transactions.stream()
                                .map(Transaction::getId)
                                .collect(Collectors.toList());

                        if (!ids.isEmpty()) {
                            query.addCriteria(Criteria.where("_id").in(ids));
                        }
                    }

                    // Query by uuid
                    if (uuid != null) {
                        query.addCriteria(Criteria.where("uuid").is(uuid));
                    }

                    // Query by email
                    if (email != null) {
                        query.addCriteria(Criteria.where("email").is(email));
                    }

                    // Query by processed status
                    if (isProcessed != null) {
                        query.addCriteria(Criteria.where("processed").is(Boolean.parseBoolean(isProcessed)));
                    }

                    // Query by cancelled status
                    if (isCancelled != null) {
                        query.addCriteria(Criteria.where("cancelled").is(Boolean.parseBoolean(isCancelled)));
                    }

                    // Query subscriptions
                    if (Boolean.parseBoolean(isSubscription)) {
                        query.addCriteria(Criteria.where("items").elemMatch(
                                Criteria.where("subscription").is(true)
                        ));
                    }

                    transactions = StaticServiceProvider.i().getMongoTemplate()
                            .find(query, Transaction.class, Constants.TRANSACTIONS_DB);

                    if (transactions.size() > 0)
                        return Response.OK;

                    return Response.NOT_FOUND;
                }
                case POST:
                case UPDATE:
                    for (Transaction transaction : transactions) {
                        Query query = Query.query(Criteria.where("_id").is(transaction.getId()));
                        Update update = StaticServiceProvider.i().getMongoUpdater().getUpdateFor(transaction);

                        StaticServiceProvider.i().getMongoTemplate().upsert(query, update, Constants.TRANSACTIONS_DB);
                    }

                    if (sendEmail) {
                        for (Transaction transaction : transactions) {
                            Map<String, Object> templateData = new HashMap<>();
                            templateData.put("date", dateFormat.format(transaction.getCreatedAt()));
                            templateData.put("username", transaction.getName());
                            double total = Arrays.stream(transaction.getItems()).mapToDouble(item -> {
                                try {
                                    return Double.parseDouble(item.getPrice()) * item.getQuantity();
                                } catch (NumberFormatException ignored) {
                                    return 0;
                                }
                            }).sum();
                            templateData.put("total", NUMBER_FORMAT.format(total));

                            // orderId
                            String orderId = transaction.getId();
                            if (orderId.contains("-")) {
                                orderId = orderId.substring(orderId.indexOf("-") + 1);
                            }
                            templateData.put("orderId", orderId);

                            // items
                            List<HashMap<String, String>> items = new ArrayList<>();
                            for (TransactionItem storeItem : transaction.getItems()) {
                                HashMap<String, String> item = new HashMap<>();

                                String description = storeItem.getDescription();
                                if (storeItem.getQuantity() > 1) {
                                    description += " (" + storeItem.getQuantity() + ")";
                                }
                                item.put("description", description);

                                item.put("subscription", String.valueOf(storeItem.isSubscription()));
                                item.put("price", storeItem.getPrice());

                                items.add(item);
                            }
                            templateData.put("items", items);

                            StaticServiceProvider.i().getEmailServices().send(
                                    EmailType.STORE_RECEIPT,
                                    transaction.getEmail(),
                                    transaction.getName(),
                                    templateData
                            );
                        }
                    }

                    return Response.OK;
            }
            return null;
        });
    }

}

package net.mineclick.ender.messenger;

import lombok.Getter;
import lombok.Setter;
import net.mineclick.core.messenger.Action;
import net.mineclick.core.messenger.Message;
import net.mineclick.core.messenger.MessageName;
import net.mineclick.core.messenger.Response;
import net.mineclick.ender.component.StaticServiceProvider;
import net.mineclick.ender.model.LeaderboardData;
import reactor.core.publisher.Flux;

import java.util.Set;
import java.util.stream.Collectors;

@Getter
@Setter
@MessageName("leaderboards")
public class LeaderboardsHandler extends Message {
    private Set<LeaderboardData> leaderboards;

    @Override
    public void onReceive() {
        wrap(action -> {
            if (action.equals(Action.GET)) {
                leaderboards = Flux.fromIterable(leaderboards)
                        .flatMap(leaderboard -> StaticServiceProvider.i().getLeaderboardsService().getLeaderboard(leaderboard.getStatistic(), leaderboard.getCount()))
                        .collect(Collectors.toSet())
                        .block();

                if (leaderboards == null || leaderboards.isEmpty())
                    return Response.NOT_FOUND;
                return Response.OK;
            }

            return Response.ERROR;
        });
    }
}

package net.mineclick.ender.messenger;

import net.mineclick.core.messenger.Action;
import net.mineclick.core.messenger.Message;
import net.mineclick.core.messenger.MessageName;
import net.mineclick.core.messenger.Response;
import net.mineclick.ender.component.StaticServiceProvider;

import java.util.Map;
import java.util.UUID;

@MessageName("textures")
public class TexturesHandler extends Message {
    private Map<UUID, String> textures;

    @Override
    public void onReceive() {
        if (getAction().equals(Action.GET)) {
            textures = StaticServiceProvider.i().getUsersService().getTextures(textures.keySet());
            send(Response.OK);
        }
    }
}

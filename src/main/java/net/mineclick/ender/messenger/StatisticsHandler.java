package net.mineclick.ender.messenger;

import lombok.Getter;
import lombok.Setter;
import net.mineclick.core.messenger.Action;
import net.mineclick.core.messenger.Message;
import net.mineclick.core.messenger.MessageName;
import net.mineclick.core.messenger.Response;
import net.mineclick.ender.component.StaticServiceProvider;
import net.mineclick.ender.model.Statistic;
import org.springframework.data.util.Pair;
import reactor.core.publisher.Flux;

import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.function.Predicate;

@Getter
@Setter
@MessageName("statistics")
public class StatisticsHandler extends Message {
    private Set<UUID> uuids;
    private Set<String> keys;
    private Map<UUID, Map<String, Statistic>> statistics;

    @Override
    public void onReceive() {
        wrap(action -> {
            if (action.equals(Action.GET)) {
                statistics = Flux.fromIterable(uuids)
                        .flatMap(uuid -> StaticServiceProvider.i().getStatisticsService().getStatistics(uuid, keys).map(map -> Pair.of(uuid, map)))
                        .collectMap(Pair::getFirst, Pair::getSecond)
                        .block();

                if (statistics == null || statistics.isEmpty())
                    return Response.NOT_FOUND;

                return Response.OK;
            }

            if (action.equals(Action.POST)) {
                Flux.fromIterable(statistics.entrySet())
                        .flatMap(entry -> StaticServiceProvider.i().getStatisticsService().setStatistics(entry.getKey(), entry.getValue()))
                        .all(Predicate.isEqual(true))
                        .block();

                return Response.OK;
            }

            return Response.ERROR;
        });
    }
}

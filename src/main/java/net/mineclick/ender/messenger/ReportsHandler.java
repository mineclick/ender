package net.mineclick.ender.messenger;

import net.mineclick.core.messenger.Message;
import net.mineclick.core.messenger.MessageName;
import net.mineclick.ender.component.StaticServiceProvider;
import net.mineclick.ender.service.DiscordService;

@MessageName("reports")
public class ReportsHandler extends Message {
    private String reporterName;
    private String reporterDiscordId;
    private String reason;
    private String punishment;
    private String offenderName;
    private String offenderUuid;

    @Override
    public void onReceive() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("`")
                .append(reporterName)
                .append("` reported `")
                .append(offenderName)
                .append("` for **")
                .append(reason)
                .append("**");

        if (punishment != null) {
            stringBuilder.append("\nPunishment: **")
                    .append(punishment)
                    .append("**");
        }

        DiscordService discord = StaticServiceProvider.i().getDiscordService();
        if (reporterDiscordId != null) {
            stringBuilder.append("\n<@")
                    .append(reporterDiscordId)
                    .append(">")
                    .append(" please provide evidence (if possible)");

        }
        discord.getChannelByName(discord.getReportsChannelName())
                .sendMessage(stringBuilder).queue();
    }
}

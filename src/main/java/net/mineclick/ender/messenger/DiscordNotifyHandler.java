package net.mineclick.ender.messenger;

import lombok.RequiredArgsConstructor;
import net.mineclick.core.messenger.Message;
import net.mineclick.core.messenger.MessageName;

@RequiredArgsConstructor
@MessageName("discordNotify")
public class DiscordNotifyHandler extends Message {
    private final String uuid;
    private final String setting;
}

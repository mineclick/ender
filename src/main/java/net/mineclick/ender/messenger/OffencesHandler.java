package net.mineclick.ender.messenger;

import lombok.Getter;
import lombok.Setter;
import net.mineclick.core.messenger.Message;
import net.mineclick.core.messenger.MessageName;
import net.mineclick.ender.model.OffenceData;

import java.util.UUID;

@Setter
@Getter
@MessageName("offences")
public class OffencesHandler extends Message {
    private UUID uuid;
    private OffenceData offenceData;
}

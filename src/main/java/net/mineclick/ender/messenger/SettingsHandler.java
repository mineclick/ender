package net.mineclick.ender.messenger;

import lombok.Setter;
import net.mineclick.core.messenger.Action;
import net.mineclick.core.messenger.Message;
import net.mineclick.core.messenger.MessageName;
import net.mineclick.core.messenger.Response;
import net.mineclick.ender.component.StaticServiceProvider;
import net.mineclick.ender.model.AnnouncementsModel;
import net.mineclick.ender.model.ConfigurationsModel;
import net.mineclick.ender.model.MotdModel;

@Setter
@MessageName("settings")
public class SettingsHandler extends Message {
    private MotdModel motd;
    private AnnouncementsModel announcements;
    private ConfigurationsModel configurations;

    @Override
    public void onReceive() {
        if (getAction().equals(Action.GET)) {
            // TODO check if this is blocking the main thread... flux subscribe should be async, but just double check
            motd = StaticServiceProvider.i().getSettingsService().getMotd();
            announcements = StaticServiceProvider.i().getSettingsService().getAnnouncements();
            configurations = StaticServiceProvider.i().getSettingsService().getConfigurations();

            send(Response.OK);
        }
    }
}

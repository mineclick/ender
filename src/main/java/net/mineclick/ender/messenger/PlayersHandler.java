package net.mineclick.ender.messenger;

import com.mongodb.client.result.UpdateResult;
import net.mineclick.core.messenger.Message;
import net.mineclick.core.messenger.MessageName;
import net.mineclick.core.messenger.Response;
import net.mineclick.ender.component.StaticServiceProvider;
import net.mineclick.ender.model.Transaction;
import net.mineclick.ender.utils.Constants;
import net.mineclick.ender.utils.DocumentBuilder;
import org.bson.Document;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;

import java.util.Date;
import java.util.List;

@MessageName("players")
public class PlayersHandler extends Message {
    private String uuid;
    private Document playerModel;

    @Override
    public void onReceive() {
        wrap(action -> {
            switch (action) {
                case GET:
                    playerModel = StaticServiceProvider.i()
                            .getMongoTemplate()
                            .getCollection(Constants.PLAYERS_DB)
                            .find(Document.parse("{_id: \"" + uuid + "\"}"))
                            .first();

                    if (playerModel != null) {
                        playerModel.put("uuid", playerModel.remove("_id"));

                        // Check the subscription status
                        String rank = playerModel.getString("rank");
                        Boolean premiumForLife = playerModel.getBoolean("premiumForLife"); // TODO don't forget to have this in the migration
                        if (rank != null && rank.equals("PAID") && (premiumForLife == null || !premiumForLife)) {
                            Query query = new Query();
                            query.addCriteria(Criteria.where("uuid").is(playerModel.getString("uuid")));
                            query.addCriteria(Criteria.where("processed").is(true));
                            query.addCriteria(Criteria.where("items").elemMatch(
                                    Criteria.where("subscription").is(true)
                            ));

                            List<Transaction> transactions = StaticServiceProvider.i().getMongoTemplate()
                                    .find(query, Transaction.class, Constants.TRANSACTIONS_DB);
                            boolean hasActive = transactions.stream().anyMatch(transaction -> {
                                if (transaction.isCancelled()) {
                                    return transaction.getNextPaymentAt() != null && transaction.getNextPaymentAt().after(new Date());
                                }

                                return true;
                            });

                            if (!hasActive) {
                                playerModel.put("rank", "DEFAULT");
                            }
                        }

                        return Response.OK;
                    }

                    return Response.NOT_FOUND;
                case POST:
                    Object id = playerModel.remove("uuid");
                    playerModel.put("_id", id);

                    // Save a snapshot
                    Date cutOffDate = new Date(System.currentTimeMillis() - 1000 * 60 * 60);
                    MongoTemplate mongoTemplate = StaticServiceProvider.i().getMongoTemplate();
                    boolean exists = mongoTemplate.exists(new Query(Criteria.where("uuid").is(id).and("date").gt(cutOffDate)), Constants.PLAYERS_SNAPSHOT_DB);
                    if (!exists) {
                        mongoTemplate
                                .getCollection(Constants.PLAYERS_SNAPSHOT_DB)
                                .insertOne(DocumentBuilder.start("uuid", id)
                                        .add("date", new Date())
                                        .add("doc", playerModel)
                                        .get()
                                );
                    }

                    // Upsert the player model
                    Update update = new Update();
                    playerModel.forEach(update::set);
                    Query query = Query.query(Criteria.where("_id").is(id));

                    UpdateResult results = mongoTemplate
                            .upsert(query, update, Constants.PLAYERS_DB);

                    return results.getModifiedCount() > 0 ? Response.OK : Response.ERROR;

            }
            return null;
        });
    }
}

package net.mineclick.ender.messenger;

import lombok.Setter;
import net.mineclick.core.messenger.Action;
import net.mineclick.core.messenger.Message;
import net.mineclick.core.messenger.MessageName;
import net.mineclick.core.messenger.Response;
import net.mineclick.ender.component.StaticServiceProvider;
import net.mineclick.ender.model.PlayerPendingData;
import net.mineclick.ender.model.PlayerWithPendingData;
import net.mineclick.ender.utils.Constants;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;

@Setter
@MessageName("votes")
public class VoteHandler extends Message {
    private String username;
    private String serviceName;

    @Override
    public void onReceive() {
        if (getAction().equals(Action.POST)) {
            if (username != null && serviceName != null) {
                PlayerWithPendingData player = StaticServiceProvider.i().getMongoTemplate()
                        .findOne(Query.query(Criteria.where("name").is(username)), PlayerWithPendingData.class, Constants.PLAYERS_DB);

                if (player != null) {
                    PlayerPendingData pendingData = player.getPendingData();
                    if (pendingData == null) {
                        pendingData = new PlayerPendingData();
                    }
                    pendingData.setVotes(pendingData.getVotes() + 1);

                    Query query = new Query(Criteria.where("_id").is(player.getUuid()));
                    Update update = new Update();
                    update.set("pendingData", pendingData);
                    StaticServiceProvider.i().getMongoTemplate().upsert(query, update, Constants.PLAYERS_DB);

                    send(Response.OK);
                    return;
                }

                send(Response.NOT_FOUND);
                return;
            }

            send(Response.ERROR);
        }
    }
}

package net.mineclick.ender.messenger;

import lombok.Getter;
import lombok.Setter;
import net.mineclick.core.messenger.Message;
import net.mineclick.core.messenger.MessageName;

@Getter
@Setter
@MessageName("message")
public class MessageHandler extends Message {
    private String uuid; // player uuid
    private String message;
}

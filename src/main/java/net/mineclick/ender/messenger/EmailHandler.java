package net.mineclick.ender.messenger;

import net.mineclick.core.messenger.Action;
import net.mineclick.core.messenger.Message;
import net.mineclick.core.messenger.MessageName;
import net.mineclick.core.messenger.Response;
import net.mineclick.ender.component.StaticServiceProvider;
import net.mineclick.ender.type.EmailType;

import java.util.Map;

@MessageName("email")
public class EmailHandler extends Message {
    private EmailType type; // required
    private String email; // required
    private String name; // optional
    private Map<String, Object> templateData; // optional

    @Override
    public void onReceive() {
        if (getAction() == null || !getAction().equals(Action.POST)) return;
        if (type == null || email == null) {
            send(Response.ERROR);
            return;
        }

        StaticServiceProvider.i().getEmailServices().send(type, email, name, templateData);
        send(Response.OK);
    }
}

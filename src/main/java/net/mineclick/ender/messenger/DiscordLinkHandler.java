package net.mineclick.ender.messenger;

import lombok.Getter;
import lombok.Setter;
import net.mineclick.core.messenger.Action;
import net.mineclick.core.messenger.Message;
import net.mineclick.core.messenger.MessageName;
import net.mineclick.ender.component.StaticServiceProvider;
import net.mineclick.ender.utils.Constants;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;

@Getter
@Setter
@MessageName("discordLink")
public class DiscordLinkHandler extends Message {
    private String uuid;
    private String discordId;

    @Override
    public void onReceive() {
        if (getAction().equals(Action.DELETE) && discordId != null) {
            StaticServiceProvider.i().getDiscordService().clearRoles(discordId);
            StaticServiceProvider.i().getMongoTemplate().updateFirst(Query.query(Criteria.where("discordId").is(discordId)), new Update().unset("discordId"), Constants.PLAYERS_DB);
        }
    }
}

/**
 * Wrap function in a periodic timer
 * @param func The function to execute
 * @param interval The interval
 */
const wrapPeriodic = (func, interval = 1000) => {
  return () => {
    func();
    const timer = setInterval(() => func(), interval);
    return () => {
      clearInterval(timer);
    };
  };
};

export {
  wrapPeriodic
};
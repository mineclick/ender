import axios from 'axios';

const getServers = () => {
  return axios.get("/api/servers");
};

export {
  getServers
};

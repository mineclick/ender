import axios from 'axios';

const getPlayer = (search) => {
  return axios.get("/api/players", { params: { search } });
};

export {
  getPlayer
};

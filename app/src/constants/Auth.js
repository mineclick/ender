const authStatusTypes = {
  awaitingOtp: "AWAITING_OTP",
  validatedOtp: "VALIDATED_OTP",
  registered: "REGISTERED"
};

export {
  authStatusTypes
};
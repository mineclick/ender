export default {
  url: (endpoint) => {
    if (process.env.NODE_ENV === "development") {
      return `http://localhost:8088/api${endpoint}`
    }
    return `/api${endpoint}`;
  }
};